#include "base_types.mligo"
#include "KYC_Registrar_types.mligo"
#include "Security_Tokens_types.mligo"
#include "Issuing_Entity_types.mligo"
#include "helpers.mligo"

let issuer_errors__not_owner = "201"                           // "Only owner can call this entrypoint"
let issuer_errors__investor_not_registered_with_unrestricted_registrar = "202"   // "This investor is not registered with an unrestricted KYC registrar known to the issuer."
let issuer_errors__investor_restricted_at_issuer_lvl = "203"   // "Investor is restricted at issuer level"
let issuer_errors__authority_err_msg = "204"                   // "You are not allowed to call this entrypoint."
let issuer_errors__token_restricted = "205"
let issuer_errors__global_investor_limit_exceeded = "206"
let issuer_errors__token_does_not_exist = "207"
let issuer_errors__global_restriction_activated = "208"
let issuer_errors__only_kyc_can_call_this_entrypoint = "209"
let issuer_errors__restricted_registrar = "210"
let issuer_errors__country_invest_limit_exceeded = "211"
let issuer_errors__registrar_contract_does_not_exist = "212"

let fail_if_sender_not_owner (s : issuing_entity_storage_t) : unit =
  if (sender <> s.owner) then failwith issuer_errors__not_owner else () [@@inline]

let update_global_limit (p, s :update_global_limit_t * issuing_entity_storage_t)
  : issuing_entity_ret_t =
  let check_sender = fail_if_sender_not_owner s in
  let new_s =
    { name = s.name ;
      owner = s.owner ;
      kyc_registrars = s.kyc_registrars ;
      security_tokens = s.security_tokens ;
      global_invest_limit = p ;
      country_restrictions = s.country_restrictions ;
      investor_counter = s.investor_counter ;
      country_counters = s.country_counters ;
      accounts = s.accounts ;
      document_hashes = s.document_hashes ;
      global_restriction = s.global_restriction ;
    } in
  ( ([] : operation list) , new_s )


let update_country_restrictions (p, s : update_country_restrictions_t * issuing_entity_storage_t)
  : issuing_entity_ret_t =
  let check_sender = fail_if_sender_not_owner s in
  let new_restrictions =
    let aux = fun (prev, el: country_restrictions_t * (country_t * country_restriction_t)) ->
      Big_map.update el.0 (Some el.1) prev
    in
    List.fold aux p s.country_restrictions
  in
  let new_s =
    { name = s.name ;
      owner = s.owner ;
      kyc_registrars = s.kyc_registrars ;
      security_tokens = s.security_tokens ;
      global_invest_limit = s.global_invest_limit ;
      country_restrictions = new_restrictions ;
      investor_counter = s.investor_counter ;
      country_counters = s.country_counters ;
      accounts = s.accounts ;
      document_hashes = s.document_hashes ;
      global_restriction = s.global_restriction ;
    } in
  ( ([] : operation list) , new_s )

let add_token (p, s : add_token_t * issuing_entity_storage_t)
  : issuing_entity_ret_t =
  let check_sender = fail_if_sender_not_owner s in
  let addable_token = p in
  let default_token_info =
    {
      restricted = false ;
    } in
  let new_security_tokens = Map.update
      addable_token (Some default_token_info) s.security_tokens in
  let new_s = {
    name = s.name ;
    owner = s.owner ;
    kyc_registrars = s.kyc_registrars ;
    security_tokens = new_security_tokens ;
    global_invest_limit = s.global_invest_limit;
    country_restrictions = s.country_restrictions ;
    investor_counter = s.investor_counter ;
    country_counters =  s.country_counters ;
    accounts = s.accounts ;
    document_hashes = s.document_hashes ;
    global_restriction = s.global_restriction ;
  } in
  ( ([] : operation list) , new_s )

let set_registrars (p, s : set_registrars_t * issuing_entity_storage_t)
  : issuing_entity_ret_t =
  let check_sender = fail_if_sender_not_owner s in
  let registrar = p.0 in
  let restricted = p.1 in
  let new_registrar_info =
    {
      restricted = restricted ;
    }
  in
  let new_kyc_registrars = Map.update registrar (Some new_registrar_info) s.kyc_registrars in
  let new_s = {
    name = s.name ;
    owner = s.owner ;
    kyc_registrars = new_kyc_registrars ;
    security_tokens = s.security_tokens ;
    global_invest_limit = s.global_invest_limit;
    country_restrictions = s.country_restrictions ;
    investor_counter = s.investor_counter ;
    country_counters =  s.country_counters ;
    accounts = s.accounts ;
    document_hashes = s.document_hashes ;
    global_restriction = s.global_restriction ;
  } in
  ( ([] : operation list) , new_s )

let set_account (p, s: set_account_t * issuing_entity_storage_t)
  : issuing_entity_ret_t =
  let check_sender = fail_if_sender_not_owner s in
  let account, new_account_info = p in
  let new_accounts = Big_map.update account (Some new_account_info) s.accounts in
  let new_s =
    { name = s.name ;
      owner = s.owner ;
      kyc_registrars = s.kyc_registrars ;
      security_tokens = s.security_tokens ;
      global_invest_limit = s.global_invest_limit ;
      country_restrictions = s.country_restrictions ;
      investor_counter = s.investor_counter ;
      country_counters = s.country_counters ;
      accounts = new_accounts ;
      document_hashes = s.document_hashes ;
      global_restriction = s.global_restriction ;
    } in
  ( ([] : operation list), new_s)

let set_token (p, s : set_token_t * issuing_entity_storage_t)
  : issuing_entity_ret_t =
  let check_sender = fail_if_sender_not_owner s in
  let token = p.0 in
  let new_restricted = p.1 in
  let token_info = match Map.find_opt token s.security_tokens with
    | Some t -> t
    | None -> (failwith issuer_errors__token_does_not_exist : token_info_t) in
  let new_token_info =
    {
      restricted = new_restricted ;
    } in
  let new_security_tokens = Map.update
      token (Some new_token_info) s.security_tokens in
  let new_s =
    { name = s.name ;
      owner = s.owner ;
      kyc_registrars = s.kyc_registrars ;
      security_tokens = new_security_tokens ;
      global_invest_limit = s.global_invest_limit ;
      country_restrictions = s.country_restrictions ;
      investor_counter = s.investor_counter ;
      country_counters = s.country_counters ;
      accounts = s.accounts ;
      document_hashes = s.document_hashes ;
      global_restriction = s.global_restriction ;
    } in
  ( ([] : operation list), new_s)


(* Private *)
let check_investors (invs, s: investor_t set * issuing_entity_storage_t) : operation list =
  let n_unrestricted_registrars =
    let aux = fun (counter, r: nat * (registrar_t * registrar_info_t)) ->
      let registrar_info = r.1 in
      if registrar_info.restricted then
        counter
      else
        counter + 1n
    in
    Map.fold aux s.kyc_registrars 0n
  in
  let check_no_registrars =
    if n_unrestricted_registrars = 0n
    then failwith issuer_errors__investor_not_registered_with_unrestricted_registrar
  in
  let aux = fun (ops, el: (operation list) * (registrar_t * registrar_info_t)) ->
    let investor_list =
      let aux = fun (acc, inv: investor_t list * investor_t) ->
        let check_inv_restriction =
          match Big_map.find_opt inv s.accounts with
          | Some inv_info ->
            if inv_info.restricted
            then failwith issuer_errors__investor_restricted_at_issuer_lvl
            else ()
          | None -> ()
        in
        inv::acc
      in
      Set.fold aux invs ([] : investor_t list)
    in
    let registrar, registrar_info = el in
    if not registrar_info.restricted then
      let contract : check_member_t contract =
        Operation.get_entrypoint "%checkMember" registrar in
      let op : operation =
        Operation.transaction (investor_list , s.country_restrictions) 0mutez contract in
      op::ops
    else
      ops
  in
  Map.fold aux s.kyc_registrars ([] : operation list)

let check_token_restriction (token_info : token_info_t) : unit =
  if  token_info.restricted then
    failwith issuer_errors__token_restricted
  else
    () [@@inline]

let check_transfer (p, s: check_transfer_t * issuing_entity_storage_t) =
  let check_global_restriction = if s.global_restriction then
      failwith issuer_errors__global_restriction_activated
  in
  let token_info = match Map.find_opt Tezos.sender s.security_tokens with
    | Some t -> t
    | None -> (failwith issuer_errors__token_does_not_exist : token_info_t) in
  let check_st_restr = check_token_restriction token_info in
  let debitor = p.0 in
  let creditor = p.1 in
  let investors = (Set.add debitor (Set.add creditor (Set.empty : investor_t set))) in
  let kyc_check = check_investors ( investors , s ) in
  ( kyc_check , s )

(* Private method *)
let get_document_hash (p, s : document_id_t * issuing_entity_storage_t)
  : document_hash_t = Big_map.find p s.document_hashes

let transfer_tokens (p, s: transfer_tokens_arg_t * issuing_entity_storage_t) =
  let kyc_check, _ = check_transfer ((p.tr_from , p.tr_to) , s) in
  let new_investor_counter =
    if p.zero_balances.sender && (not p.zero_balances.receiver) then
      relu (s.investor_counter , 1n)
    else if (not p.zero_balances.sender) && p.zero_balances.receiver then
      s.investor_counter + 1n
    else if p.zero_balances.sender && p.zero_balances.receiver then
      s.investor_counter
    else
      s.investor_counter
  in
  let check_inv_gloabl_count = require
      (new_investor_counter <= s.global_invest_limit)
      issuer_errors__global_investor_limit_exceeded
  in
  let new_s =
    { name = s.name ;
      owner = s.owner ;
      kyc_registrars = s.kyc_registrars ;
      security_tokens = s.security_tokens ;
      global_invest_limit = s.global_invest_limit ;
      country_restrictions = s.country_restrictions ;
      investor_counter = new_investor_counter ;
      country_counters = s.country_counters ;
      accounts = s.accounts ;
      document_hashes = s.document_hashes ;
      global_restriction = s.global_restriction ;
    } in
  (kyc_check , new_s)

let modify_token_total_supply (p, s: modify_token_total_supply_arg_t * issuing_entity_storage_t) =
  let kyc_checks, _ = check_transfer ((p.investor , p.investor) , s) in
  let new_investor_counter =
    if p.was_balance_zero && (not p.is_balance_zero) then
      (s.investor_counter + 1n)
    else if (not p.was_balance_zero) && p.is_balance_zero then
      relu (s.investor_counter , 1n)
    else
      s.investor_counter
  in
  let new_s =
    { name = s.name ;
      owner = s.owner ;
      kyc_registrars = s.kyc_registrars ;
      security_tokens = s.security_tokens ;
      global_invest_limit = s.global_invest_limit ;
      country_restrictions = s.country_restrictions ;
      investor_counter = new_investor_counter ;
      country_counters = s.country_counters ;
      accounts = s.accounts ;
      document_hashes = s.document_hashes ;
      global_restriction = s.global_restriction ;
    } in
  (kyc_checks , new_s)

let remove_country_restrictions (country, s: remove_country_restrictions_arg_t * issuing_entity_storage_t) =
  let new_country_restrictions = Big_map.remove country s.country_restrictions in
  let new_s =
    { name = s.name ;
      owner = s.owner ;
      kyc_registrars = s.kyc_registrars ;
      security_tokens = s.security_tokens ;
      global_invest_limit = s.global_invest_limit ;
      country_restrictions = new_country_restrictions ;
      investor_counter = s.investor_counter ;
      country_counters = s.country_counters ;
      accounts = s.accounts ;
      document_hashes = s.document_hashes ;
      global_restriction = s.global_restriction ;
    } in
  (([] : operation list) , new_s)

let set_global_restriction (new_global_restriction, s : set_global_restriction_arg_t * issuing_entity_storage_t) =
  let new_s =
    { name = s.name ;
      owner = s.owner ;
      kyc_registrars = s.kyc_registrars ;
      security_tokens = s.security_tokens ;
      global_invest_limit = s.global_invest_limit ;
      country_restrictions = s.country_restrictions ;
      investor_counter = s.investor_counter ;
      country_counters = s.country_counters ;
      accounts = s.accounts ;
      document_hashes = s.document_hashes ;
      global_restriction = new_global_restriction ;
    } in
  (([] : operation list) , new_s)

let update_country_counter (p , s : update_country_counter_arg_t * issuing_entity_storage_t) =
  let check_sender_auth = match Map.find_opt Tezos.sender s.kyc_registrars with
    | Some kyc -> if kyc.restricted then failwith issuer_errors__restricted_registrar else ()
    | None -> failwith issuer_errors__only_kyc_can_call_this_entrypoint
  in

  (* Since this method is called from the KYC registrar after doing the country
     restriction checks, we skip them here. *)

  let new_country_counters =
    let aux = fun (country_counters, info: country_counters_t * check_member_cb_info_t ) ->
      let default_country_counter = {
        global_counter = 0n ;
        rating_counter = (Map.literal [] : rating_counter_t) ;
      } in
      let country_counter = match Big_map.find_opt info.country country_counters with
        | Some c -> c
        | None -> default_country_counter
      in
      let new_global_counter = match info.decrement with
        | false -> country_counter.global_counter + 1n
        | true -> relu (country_counter.global_counter , 1n)
      in
      let country_restriction = Big_map.find info.country s.country_restrictions in
      let check_country_invest_limit = require (new_global_counter <= country_restriction.country_invest_limit) issuer_errors__country_invest_limit_exceeded in
      let new_country_counter = {
        global_counter = new_global_counter ;
        rating_counter = country_counter.rating_counter ;
      } in
      Big_map.update info.country (Some new_country_counter) country_counters
    in
    List.fold aux p s.country_counters
  in
  let new_s =
    { name = s.name ;
      owner = s.owner ;
      kyc_registrars = s.kyc_registrars ;
      security_tokens = s.security_tokens ;
      global_invest_limit = s.global_invest_limit ;
      country_restrictions = s.country_restrictions ;
      investor_counter = s.investor_counter ;
      country_counters = new_country_counters ;
      accounts = s.accounts ;
      document_hashes = s.document_hashes ;
      global_restriction = s.global_restriction ;
    } in
  (([] : operation list) , new_s)

let main (param, s : issuing_entity_ep_t * issuing_entity_storage_t)
  : issuing_entity_ret_t =
  match param with
  | AddToken p -> add_token (p , s)
  | SetRegistrar p -> set_registrars (p , s)
  | SetAccount p -> set_account (p , s)
  | SetToken p -> set_token (p , s)
  | CheckTransfer p -> check_transfer (p , s)
  | TransferTokens p -> transfer_tokens (p , s)
  | ModifyTokenTotalSupply p -> modify_token_total_supply (p , s)
  | RemoveCountryRestrictions p -> remove_country_restrictions (p , s)
  | SetGlobalRestriction p -> set_global_restriction (p , s)
  | UpdateGlobalLimit p -> update_global_limit (p , s)
  (* To test *)
  | UpdateCountryRestrictions p -> update_country_restrictions (p , s)
