(**** storage types ****)


type registrar_info_t = {
  restricted : restricted_t ;
}
type registrars_t = (registrar_t , registrar_info_t) map
type token_info_t = {
  restricted : restricted_t ;
}
type tokens_t = (token_t , token_info_t) map
type rating_counter_t  = (rating_t  , counter_t) map
type country_counter_t = {
  global_counter : counter_t ;
  rating_counter : rating_counter_t ;
}
type country_counters_t = (country_t , country_counter_t) big_map
type rating_restriction_t  = (rating_t  , limit_t) map
type country_restriction_t = {
  country_invest_limit : limit_t ;
  min_rating   : rating_t ;
  rating_restrictions : rating_restriction_t ;
}
type country_restrictions_t = (country_t, country_restriction_t) big_map

type account_info_t = {
  registrar : registrar_t ;
  restricted : restricted_t ;
}
type account_t = address
type accounts_t = (account_t , account_info_t) big_map

type document_id_t = string
type document_hash_t = bytes
type document_hashes_t = (document_id_t , document_hash_t) big_map

type issuing_entity_storage_t = {
  name            : name_t ;
  owner           : owner_t ;
  kyc_registrars  : registrars_t ;
  security_tokens : tokens_t ;
  accounts        : accounts_t ;
  (*restrictions*)
  global_invest_limit : limit_t ;
  country_restrictions : country_restrictions_t ;
  global_restriction : restricted_t ;
  (*counters*)
  investor_counter : counter_t ;
  country_counters  : country_counters_t ;
  document_hashes : document_hashes_t ;
}


(**** I/O types ****)

(** other **)

type update_global_limit_t = limit_t
type update_country_restrictions_t = (country_t * country_restriction_t) list
type add_token_t = token_t
type set_registrars_t = registrar_t * restricted_t
type set_account_t = account_t * account_info_t
type set_token_t = token_t * restricted_t
type check_transfer_t = investor_t * investor_t
type remove_country_restrictions_arg_t = country_t
type zero_balances_t = {
  sender: bool ;  (* is sender balance now zero? *)
  receiver: bool ;  (* was receiver balance zero? *)
}
type transfer_tokens_arg_t = {
  tr_to : investor_t ;
  tr_from : investor_t ;
  zero_balances : zero_balances_t ;
}
type modify_token_total_supply_arg_t = {
  was_balance_zero : bool ;
  is_balance_zero : bool ;
  investor : investor_t ;
}

type set_global_restriction_arg_t = restricted_t

type decrement_t = bool
type check_member_cb_info_t = {
  country : country_t ;
  decrement : decrement_t ;
}
type update_country_counter_arg_t = check_member_cb_info_t list

type issuing_entity_ep_t =
  | UpdateGlobalLimit of update_global_limit_t
  | UpdateCountryRestrictions of update_country_restrictions_t
  | AddToken of add_token_t
  | SetRegistrar of set_registrars_t
  | SetAccount of set_account_t
  | SetToken of set_token_t
  | CheckTransfer of check_transfer_t
  | TransferTokens of transfer_tokens_arg_t
  | ModifyTokenTotalSupply of modify_token_total_supply_arg_t
  | RemoveCountryRestrictions of remove_country_restrictions_arg_t
  | SetGlobalRestriction of set_global_restriction_arg_t

type issuing_entity_ret_t = (operation list * issuing_entity_storage_t)
