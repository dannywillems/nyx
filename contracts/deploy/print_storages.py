import json
import subprocess

import pytest_tezos.TestTypes as t

from pynyx import helpers as h, storages


storages = {
    "kyc": storages.KYCStorage(
        owner=t.Address(h.ZERO_ADDRESS),
        members=h.empty_map(),
    ),
    "issuer": storages.IssuingEntityStorage(
        name=t.String("name"),
        owner=t.Address(h.ZERO_ADDRESS),
        kyc_registrars=h.empty_map(),
        security_tokens=h.empty_map(),
        global_invest_limit=t.Nat(1),
        country_restrictions=h.empty_map(),
        investor_counter=h.empty_map(),
        country_counters=t.Nat(2),
        accounts=h.empty_map(),
        document_hashes=h.empty_map(),
        global_restriction=t.Bool(True),
    ),
    "token": storages.SecurityTokensStorage(
        name=t.String("name"),
        allow_transfer_from=h.empty_map(),
        issuer=t.Address(h.ZERO_ADDRESS),
        tokens=t.Nat(1),
        agreement_procedure=t.Bool(True),
        balances=h.empty_map(),
        symbol=t.String("symbol"),
        owner=t.Address(h.ZERO_ADDRESS),
        total_supply=t.Nat(2),
    ),
    "mini-nyx": storages.MiniNyxStorage(
        name=t.String("name"),
        allow_transfer_from=h.empty_map(),
        tokens=t.Nat(1),
        balances=h.empty_map(),
        symbol=t.String("symbol"),
        owner=t.Address(h.ZERO_ADDRESS),
        total_supply=t.Nat(2),
    ),
    "dvp": storages.DvPStorage(
        owner=t.Address(h.ZERO_ADDRESS),
        order_book=h.empty_map(),
        counter=t.Nat(0),
    ),
}

for contract_name, storage in storages.items():
    cmd = [
        "ligo",
        "compile-expression",
        "cameligo",
        "--michelson-format=json",
        storage.__repr__(),
    ]
    res = subprocess.check_output(cmd).decode("utf8")
    print("Contract " + contract_name)
    print("  |-> Ligo:      " + storage.__repr__())
    print("  |-> Michelson: " + json.loads(res).__str__())
    print("")
