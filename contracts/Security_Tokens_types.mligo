(**** storage types ****)

type approval_t = {
  ap_from : investor_t ;
  ap_to : investor_t ;
  expires : epoch_time_t ;
  tokens : amount_t ; (*amount allowed*)
}
type investor_amount_t = ( investor_t , amount_t ) big_map

type transfer_from_info_t = {
  transfer_for : investor_t ;
  amount : amount_t ;
}
//type allow_transfer_from_t = ( authority_t , transfer_from_info_t ) big_map
type allow_transfer_from_t = ( (investor_t * authority_t) , amount_t ) big_map

type symbol_t = string
type decimals_t = nat
type owner_id_t = bytes
type standalone_t = bool
type security_token_storage_t = {
  name : name_t ;
  allow_transfer_from : allow_transfer_from_t ;
  symbol : symbol_t ;
  owner : owner_t ;
  issuer : issuer_t ;
  tokens : amount_t ; (*number of initial tokens*)
  total_supply : amount_t ;
  agreement_procedure : agreement_procedure_t ;
  balances : investor_amount_t ;
}

(**** I/O types ****)

type set_allow_transfer_from__t = {
  authority : authority_t ;
  transfer_from_info : transfer_from_info_t ;
  remove : bool ;
}
type set_allow_transfer_from_t = {
  authority : authority_t ;
  transfer_from_info : transfer_from_info_t ;
  remove : bool ;
  prev_amount : amount_t ;
}

type transfer_from_el_t = {
  tr_from : investor_t ;
  tr_to : investor_t ;
  amount : amount_t ;
}
type transfer_from_t = transfer_from_el_t list

type transfer_el_t = {
  tr_to : investor_t ;
  amount : amount_t ;
}
type transfer_t = transfer_el_t list

type burn_t = {
  tr_to : investor_t ;
  amount : amount_t ;
}
type mint_t = {
  tr_to : investor_t ;
  amount : amount_t ;
}
type security_token_ep_t =
  | Transfer of transfer_t
  | Burn of burn_t
  | Mint of mint_t
  | SetAllowTransferFrom of set_allow_transfer_from_t
  | TransferFrom of transfer_from_t

type security_token_ret_t = (operation list * security_token_storage_t)

