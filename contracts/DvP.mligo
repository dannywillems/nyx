#include "base_types.mligo"
#include "helpers.mligo"
#include "DvP_types.mligo"
#include "Security_Tokens_types.mligo"


let dvp_errors__order_does_not_exist = "61"
let dvp_errors__order_deadline_passed = "62"
let dvp_errors__not_enough_tz = "63"
let dvp_errors__take_order_not_allowed = "64"

let make_order (order_info, s: order_info_t * dvp_storage_t) =
  let is_from_tz =
    if order_info.token_from = zero_address then true else false
  in
  let check_enough_tz = if is_from_tz then require (Tezos.amount = order_info.amount_from * 1tz) dvp_errors__not_enough_tz in
  let order_id = s.counter in
  let new_order_book = Big_map.update order_id (Some (order_info , order_id)) s.order_book in
  let new_s = {
    owner = s.owner ;
    order_book = new_order_book ;
    counter = s.counter + 1n ;
  } in
  (([] : operation list) , new_s)

let take_order (p, s: take_order_arg_t * dvp_storage_t) =
  let is_allowed =
    if (
      sender <> s.owner
      && sender <> p.take_for
    ) then failwith dvp_errors__take_order_not_allowed else unit
  in
  let order_info, _ = match Big_map.find_opt p.order_id s.order_book with
    | Some o -> o
    | None -> (failwith dvp_errors__order_does_not_exist : order_info_t * order_id_t)
  in
  let check1 = require (order_info.deadline >= Tezos.now) dvp_errors__order_deadline_passed in

  let is_to_tz =
    if order_info.token_to = zero_address then true else false
  in
  let is_from_tz =
    if order_info.token_from = zero_address then true else false
  in

  let transaction_A =
    if is_from_tz then
      let receiver_contract =
        match (Tezos.get_contract_opt p.take_for : unit contract option) with
        | Some contract -> contract
        | None -> (failwith "No contract" : unit contract)
      in
      Tezos.transaction unit (order_info.amount_from * 1tz) receiver_contract
    else
      let contract_A : transfer_from_t contract = Operation.get_entrypoint "%transferFrom" order_info.token_from in
      Operation.transaction [{ tr_from = order_info.sender ; tr_to = p.take_for ; amount = order_info.amount_from ; }] 0mutez contract_A
  in
  let transaction_B =
    if is_to_tz then
      let receiver_contract =
        match (Tezos.get_contract_opt order_info.sender : unit contract option) with
        | Some contract -> contract
        | None -> (failwith "No contract" : unit contract)
      in
      let check_sent_enough_tz = require (Tezos.amount = order_info.amount_to * 1tz) dvp_errors__not_enough_tz in
      Tezos.transaction unit (order_info.amount_to * 1tz) receiver_contract
    else
      let contract_B : transfer_from_t contract = Operation.get_entrypoint "%transferFrom" order_info.token_to in
      Operation.transaction [{ tr_from = p.take_for ; tr_to = order_info.sender ; amount = order_info.amount_to ; }] 0mutez contract_B
  in
  let new_s = {
    owner = s.owner ;
    order_book = Big_map.remove p.order_id s.order_book ;
    counter = s.counter ;
  } in
  (transaction_A :: [transaction_B] , new_s)

let main (param, s: dvp_ep_t * dvp_storage_t) : dvp_ret_t =
  match param with
  | MakeOrder p -> make_order (p , s)
  | TakeOrder p -> take_order (p , s)
