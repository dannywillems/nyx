type module_t = address
type entrypoint_t = string
type permission_t = bool
type active_t = bool

type hook_info_t = {
  tag_bools : bool list ;
  permitted : permission_t ;
  active : active_t ;
  always : bool ;
}
let create_hook_info (t: bool list) (p: permission_t) (ac: active_t) (al: bool)
    : hook_info_t =
  {
    tag_bools = t ;
    permitted = p ;
    active = ac ;
    always = al ;
  }

type hooks_t = (entrypoint_t , hook_info_t) map
type permissions_t = (entrypoint_t , permission_t) map
type module_info_t = {
  active : active_t ;
  set : bool ;
  hooks : hooks_t;
  permissions : permissions_t;
}

let create_module_info (a: active_t) (s : bool) (h : hooks_t) (p: permissions_t)
    : module_info_t =
  {
    active = a ;
    set = s ;
    hooks = h ;
    permissions = p ;
  }

type active_modules_t = module_t set
type module_data_t = (module_t , module_info_t) map

type value_t = int
type storage_t = {
  value : value_t ;

  (* Modules *)
  active_modules : active_modules_t ;
  module_data : module_data_t ;
}

type return_t = (operation list) * storage_t

let storage_from_value (v: value_t) (s: storage_t): storage_t = {
  value = v ;
  active_modules = s.active_modules ;
  module_data = s.module_data ;
}

let storage_from_active_modules (a: module_t set) (s: storage_t): storage_t = {
  value = s.value ;
  active_modules = a ;
  module_data = s.module_data ;
}

let storage_from_module_data (m: module_data_t) (s: storage_t): storage_t = {
  value = s.value ;
  active_modules = s.active_modules ;
  module_data = m ;
}

type set_module_hooks_t = entrypoint_t set
let set_module_hooks (es: set_module_hooks_t) (s: storage_t): return_t =
  let m = sender in
  let new_hooks =
    let aux = fun (acc: hooks_t) (el: entrypoint_t) ->
      let new_hook_info = create_hook_info ([] : bool list) true true true in
      Map.update el (Some new_hook_info) acc
    in
    Set.fold aux es (Map.literal [] : hooks_t)
  in
  let module_info = Map.find m s.module_data in
  let new_module_info = create_module_info module_info.active module_info.set new_hooks module_info.permissions in
  let new_module_data = Map.update m (Some new_module_info) s.module_data in
  let new_storage = storage_from_module_data new_module_data s in
  (([] : operation list), new_storage)

let activate_module (m: module_t) (s: storage_t): return_t =
  let permissions = (Map.literal [] : permissions_t) in
  let hooks = (Map.literal [] : hooks_t) in
  let module_info = create_module_info true true hooks permissions in
  let new_module_data = Map.update m (Some module_info) s.module_data in
  let new_active_modules = Set.add m s.active_modules in
  let new_storage = {
    value = s.value ;
    active_modules = new_active_modules ;
    module_data = new_module_data ;
  } in

  (* Have the module call increment.set_module_hooks *)
  let contract : unit contract = Operation.get_entrypoint "%setModuleHooks" m in
  let op : operation = Operation.transaction () 1mutez contract in
  ([op], new_storage)

let modular_increment (a: unit) (s: storage_t): return_t =
  let new_storage = storage_from_value (s.value + 1) s in

  let ops =
    let aux = fun (acc: operation list) (el: module_t) ->
      let contract : unit contract = Operation.get_entrypoint "%increment" el in
      let op : operation = Operation.transaction () 1mutez contract in
      op :: acc in
    Set.fold aux s.active_modules ([] : operation list)
  in

  (ops, new_storage)

let increment (a: unit) (s: storage_t): return_t =
  let new_storage = storage_from_value (s.value + 1) s in
  (([] : operation list), new_storage)

let subtract_value (a: value_t) (s: storage_t): return_t =
  let new_storage = storage_from_value (s.value - a) s in
  (([] : operation list), new_storage)

type action =
  | Increment of unit
  | ModularIncrement of unit
  | Decrement of unit
  | ActivateModule of module_t
  | SetModuleHooks of set_module_hooks_t

let main (p : action) (s : storage_t): return_t =
  match p with
  | Increment n -> increment () s
  | ModularIncrement n -> modular_increment () s
  | Decrement n -> subtract_value 1 s
  | ActivateModule n -> activate_module n s
  | SetModuleHooks n -> set_module_hooks n s
