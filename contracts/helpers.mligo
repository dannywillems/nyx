let require (cond: bool) (msg: string) : unit = if cond then () else failwith(msg) [@@inline]
let no_op = ( [] : operation list )

let zero_address = ("tz1burnburnburnburnburnburnburjAYjjX" : address)

let relu (a, b: nat * nat) : nat = if (a - b) >= 0 then abs (a - b) else 0n [@@inline]
