from pytest_tezos.TestTypes import Address, Map, Nat

from pynyx.environments import TokenEnv, EnvParams
from pynyx.contracts import Token as TokenContract, TokenParams

from Issuing_Entity__tests import check_permissions, Env as IssuerEnv
from helpers import _test_is_owner, _test_error_msg, assert_is_not_in_big_map, map_contract_addr_to_type
from tests.pytest_tezos import tezos

from pytezos.operation import fees
fees.hard_gas_limit_per_operation = 1040000

gas_ops = []

def addr_to_contract_type(env):
    return {
        env.kyc.addr: 'kyc',
        env.issuer.addr: 'issuer',
        env.token.addr:  'token',
    }


class Token(TokenContract):
    def init_storage(self):
        init_storage = super().init_storage()
        #init_storage.balances = Map({Address(self.tezos.addresses[0]): Nat(100)})
        init_storage.tokens = Nat(200)
        return init_storage


class Env(TokenEnv):
    def __init__(
            self,
            ligo,
            tezos,
            params: EnvParams = None,
        ):
        super().__init__(
            ligo,
            tezos,
            params,
            TokenContract=Token,
            ParentEnv=IssuerEnv,
        )

    def setup_env(self, arg, mint=True):
        super().setup_env(arg)

        if mint:
            self.tezos.wait(self.token.ci.mint({'amount': 100, 'tr_to': arg[0]}))

        return self


def test_is_owner(ligo, tezos):
    """Tests that restricted entrypoints can only be called from contract owner."""
    err_msg = "31"

    entrypoints = [
        ["mint", {'amount': 12, 'tr_to': tezos.addresses[0]}],
        ["burn", {'amount': 12, 'tr_to': tezos.addresses[0]}],
        ["setAllowTransferFrom", {
            'authority': tezos.addresses[0],
            'transfer_from_info': {'transfer_for': tezos.addresses[0], 'amount': 1},
            'remove': False
            }
         ],
    ]

    e = Env(ligo, tezos)
    _test_is_owner(ligo, tezos, entrypoints[:2], err_msg, ci=e.token.ci)

    del(entrypoints[0])
    del(entrypoints[1])
    _test_is_owner(ligo, tezos, entrypoints[2:], 'You are not allowed to allow transfer from for this investor', ci=e.token.ci)

    gas_ops.append(map_contract_addr_to_type(e, addr_to_contract_type))

Approval = lambda f, t, e, n: {
    "ap_from" : f,
    "ap_to" : t,
    "expires" : e,
    "tokens" : n,
}

def test_mint(ligo, tezos):
    """Test that the issuer can mint tokens."""
    inv = tezos.addresses[4]
    e = Env(ligo, tezos).setup_env([inv], False)

    # Test that owner can mint to accredited investor
    def mint(amount = 12):
        try:
            balance_before = e.token.ci.big_map_get('balances/'+inv)
        except:
            balance_before = 0
        total_supply_before = e.token.ci.storage()['total_supply']
        tezos.wait(e.token.ci.mint({'amount': amount, 'tr_to': inv}))
        assert e.token.ci.big_map_get('balances/' + inv) == balance_before + amount
        assert e.token.ci.storage()['total_supply'] == total_supply_before + amount


    # Minting should work
    prev_counter = e.issuer.ci.storage()['investor_counter']
    mint()
    assert e.issuer.ci.storage()['investor_counter'] == prev_counter + 1
    country = e.kyc.ci.big_map_get('members/' + inv)["country"]

    # Trying to mint more than is available
    cb = lambda: mint(e.token.ci.storage()['tokens'] + 1)
    _test_error_msg(cb, "34")

    # Checking investor permissions for minting to
    check_permissions(e, mint, inv)

    gas_ops.append(map_contract_addr_to_type(e, addr_to_contract_type))

def test_burn(ligo, tezos):
    """Test that the issuer can burn tokens."""
    inv = tezos.addresses[2]
    e = Env(ligo, tezos).setup_env([inv], False)

    init_amount = 10
    tezos.wait(e.token.ci.mint({'amount': init_amount, 'tr_to': inv}))

    # Test that owner can burn to accredited investor
    def burn(amount = 1):
        balance_before = e.token.ci.big_map_get('balances/'+inv)
        total_supply_before = e.token.ci.storage()['total_supply']
        tezos.wait(e.token.ci.burn({'amount': amount, 'tr_to': inv}))
        assert e.token.ci.big_map_get('balances/'+inv) == balance_before - amount
        assert e.token.ci.storage()['total_supply'] == total_supply_before - amount

    # Burning should work
    prev_counter = e.issuer.ci.storage()['investor_counter']
    burn(e.token.ci.big_map_get('balances/'+inv))
    assert e.issuer.ci.storage()['investor_counter'] == prev_counter - 1

    tezos.wait(e.token.ci.mint({'amount': init_amount, 'tr_to': inv}))

    # Trying to burn more than is available
    cb = lambda: burn(e.token.ci.big_map_get('balances/'+inv) + 1)
    _test_error_msg(cb, "33")

    # Checking investor permissions for burning to
    check_permissions(e, burn, inv)

    gas_ops.append(map_contract_addr_to_type(e, addr_to_contract_type))


def allow_transfer_from(e, authority, transfer_for, amount, remove=False):
    set_allow_transfer_from_params = e.token.make_set_allow_transfer_from_params(
        authority, transfer_for, amount, remove)

    e.tezos.wait(e.token.ci.setAllowTransferFrom(set_allow_transfer_from_params))

    return set_allow_transfer_from_params['transfer_from_info']

def test_set_allow_transfer_from(ligo, tezos):
    """Tests that the owner can add an authority allowed to call transfer_from."""
    authority_index = 4
    authority = tezos.addresses[authority_index]
    e = Env(ligo, tezos)

    def set_allow_transfer_from(authority, transfer_for, amount, remove):
        allow_transfer_from(e, authority, transfer_for, amount, remove)
        if remove:
            assert_is_not_in_big_map(e.token.ci, 'allow_transfer_from', authority)
        else:
            assert e.token.get_allowance_for(authority, transfer_for) == amount

    set_allow_transfer_from(authority, tezos.addresses[1], 1000, False)
    set_allow_transfer_from(authority, tezos.addresses[1], 1000, True)

    transfer_from_info = {
        'transfer_for': tezos.addresses[1],
        'amount': 1000,
    }
    set_allow_transfer_from_params = {
        'authority': authority,
        'transfer_from_info': transfer_from_info,
        'remove': False,
        'prev_amount': 123456789,
    }
    cb = lambda: e.tezos.wait(e.token.ci.setAllowTransferFrom(set_allow_transfer_from_params))
    # Should pass because allowance does not exist yet
    cb()
    # Should fail because prev_amount different
    _test_error_msg(cb, "38")

    gas_ops.append(map_contract_addr_to_type(e, addr_to_contract_type))

def test_transfer(ligo, tezos):
    """Test that the issuer can transfer tokens."""
    sender_index = 0

    inv_from = tezos.addresses[sender_index]
    inv_to, inv_to_client = tezos.addresses[1], tezos.clients[1]
    extra_inv, extra_inv_client = tezos.addresses[2], tezos.clients[2]
    e = Env(ligo, tezos).setup_env([inv_from, inv_to, extra_inv])


    token_client = e.tezos.clients[sender_index]
    e.issuer.ci.checkTransfer.key = token_client.key

    # Test that accredited investor can transfer to accredited investor
    def transfer(amount = 22):
        balance_from_before = e.token.ci.big_map_get('balances/'+inv_from)

        try:
            balance_to_before = e.token.ci.big_map_get('balances/'+inv_to)
        except:
            balance_to_before = 0

        tezos.wait(e.token.ci.transfer([{
            'tr_to': inv_to,
            'amount': amount,
        }]))
        assert e.token.ci.big_map_get('balances/'+inv_from) == balance_from_before - amount
        assert e.token.ci.big_map_get('balances/'+inv_to) == balance_to_before + amount

    # Transferring should work
    prev_counter = e.issuer.ci.storage()['investor_counter']
    transfer()
    assert e.issuer.ci.storage()['investor_counter'] == prev_counter + 1

    # Trying to transfer more than is available
    cb = lambda: transfer(e.token.ci.big_map_get('balances/'+inv_from) + 1)
    _test_error_msg(cb, "32")

    # Checking investor permissions for transferring to
    check_permissions(e, transfer, inv_from)

    # Checking that the issuer investor counter works properly in edge cases
    cb = lambda: tezos.wait(e.token.ci.transfer([{'tr_to': extra_inv, 'amount': 1}]))
    _test_error_msg(cb, "206")

    prev_counter = e.issuer.ci.storage()['investor_counter']
    e.token.ci.transfer.key = inv_to_client.key
    tezos.wait(e.token.ci.transfer([{'tr_to': extra_inv, 'amount': e.token.ci.big_map_get('balances/'+inv_to)}]))
    assert e.issuer.ci.storage()['investor_counter'] == prev_counter

    prev_counter = e.issuer.ci.storage()['investor_counter']
    e.token.ci.transfer.key = extra_inv_client.key
    tezos.wait(e.token.ci.transfer([{'tr_to': inv_from, 'amount': e.token.ci.big_map_get('balances/'+extra_inv)}]))
    assert e.issuer.ci.storage()['investor_counter'] == prev_counter - 1

    gas_ops.append(map_contract_addr_to_type(e, addr_to_contract_type))


def test_transfer_from(ligo, tezos):
    """Test that the issuer can transfer tokens."""
    authority_index = 1
    tr_from_index = 0
    tr_to_index = 2
    extra_inv = tezos.addresses[3]

    authority = tezos.addresses[authority_index]
    inv_from = tezos.addresses[tr_from_index]
    inv_to = tezos.addresses[tr_to_index]
    e = Env(ligo, tezos).setup_env([inv_from, inv_to, extra_inv])

    authority_client = e.tezos.clients[authority_index]
    e.token.ci.transferFrom.key = authority_client.key

    # Test that accredited investor can transfer to accredited investor
    def transfer_from(amount = 22, inv_from = inv_from):
        balance_from_before = e.token.ci.big_map_get('balances/'+inv_from)
        try:
            balance_to_before = e.token.ci.big_map_get('balances/'+inv_to)
        except:
            balance_to_before = 0

        tezos.wait(e.token.ci.transferFrom([{
            'tr_from': inv_from,
            'tr_to': inv_to,
            'amount': amount,
        }]))

        assert e.token.ci.big_map_get('balances/'+inv_from) == balance_from_before - amount
        assert e.token.ci.big_map_get('balances/'+inv_to) == balance_to_before + amount

    ## Happy path

    amount = 20
    allow_transfer_from(e, authority, inv_from, amount, False)
    prev_counter = e.issuer.ci.storage()['investor_counter']
    transfer_from(amount)
    assert e.issuer.ci.storage()['investor_counter'] == prev_counter + 1
    assert e.token.get_allowance_for(authority, inv_from) == 0

    amount = 20
    allow_transfer_from(e, authority, inv_from, amount, False)
    answer = int(amount / 2)
    prev_counter = e.issuer.ci.storage()['investor_counter']
    transfer_from(answer)
    assert e.issuer.ci.storage()['investor_counter'] == prev_counter
    assert e.token.get_allowance_for(authority, inv_from) == answer

    ## Fail cases

    cb = lambda: transfer_from(e.token.ci.big_map_get('balances/'+inv_from) + 1)
    _test_error_msg(cb, "37")

    # Remove the authorization for authority
    allow_transfer_from(e, authority, inv_from, amount, True)
    cb = lambda: transfer_from(amount, inv_from)
    _test_error_msg(cb, "36")

    # Checking investor permissions for transferring to
    allow_transfer_from(e, authority, inv_from, 100000, False)
    check_permissions(e, transfer_from, inv_from)

    # Issuer investor counter edge cases

    cb = lambda: tezos.wait(e.token.ci.transferFrom([{'tr_from': inv_from, 'tr_to': extra_inv, 'amount': 1,}]))
    _test_error_msg(cb, "206")

    amount = e.token.ci.big_map_get("balances/"+inv_from)
    prev_counter = e.issuer.ci.storage()["investor_counter"]
    transfer_from(amount)
    assert e.issuer.ci.storage()["investor_counter"] == prev_counter - 1

    gas_ops.append(map_contract_addr_to_type(e, addr_to_contract_type))

def test_save_gas_costs():
    import json
    with open('gas/token.json', 'w', encoding='utf-8') as f:
        json.dump(gas_ops, f, ensure_ascii=False, indent=4)
