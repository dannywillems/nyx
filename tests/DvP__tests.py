from pynyx.environments import DvPEnv, EnvParams
from pynyx.contracts import DvP, DvPParams, Timestamp, TokenParams, MiniNyx, MiniNyxParams
from datetime import datetime
from Security_Tokens__tests import Env as TokenEnv

from helpers import _test_is_owner, _test_error_msg, assert_is_not_in_big_map, map_contract_addr_to_type
from tests.pytest_tezos import tezos
from pynyx.helpers import ZERO_ADDRESS

from decimal import Decimal


from pytezos.operation import fees
fees.hard_gas_limit_per_operation = 1040000


# Stand alone tokens

gas_ops = []

def addr_to_contract_type_mini(env):
    return {
        env.tokenA.addr: 'mini_nyx',
        env.tokenB.addr: 'mini_nyx',
        env.dvp.addr: 'mini_dvp',
    }

class MiniEnv(DvPEnv):
    def setup_env(self, alice, bob, bob_amount, alice_amount):
        def get_token():
            owner = self.dvp.ci.storage()['owner']
            token_params = MiniNyxParams(owner=owner)
            return MiniNyx(self.ligo, self.tezos, token_params)

        self.tokenA = get_token()
        self.tokenB = get_token()

        self.tezos.wait(self.tokenA.ci.mint({'amount': alice_amount, 'tr_to': alice}))
        self.tezos.wait(self.tokenB.ci.mint({'amount': bob_amount, 'tr_to': bob}))


def test_make_order_mini(ligo, tezos):
    dvp_params = DvPParams(owner=tezos.addresses[0])
    params = EnvParams(DvP=dvp_params)
    e = MiniEnv(ligo, tezos, params)

    # Token to Token
    order = {
        "amount_from": 10,
	    "amount_to": 20,
	    "deadline": datetime(2020, 5, 24).strftime("%Y-%m-%dT%H:%M:%SZ"),
	    "sender": tezos.addresses[0],
	    "token_from": tezos.addresses[1],
	    "token_to": tezos.addresses[2],
    }
    init_order_counter = e.dvp.ci.storage()['counter']
    resp = tezos.wait(e.dvp.ci.makeOrder(order))
    order_id = resp['contents'][0]['metadata']['operation_result']['big_map_diff'][0]['key']['int']
    e.dvp.ci.big_map_get('order_book/' + order_id)
    assert {**order, 'nat_6': init_order_counter} == e.dvp.ci.big_map_get('order_book/' + order_id)
    assert e.dvp.ci.storage()['counter'] == init_order_counter + 1

    # Tz to Token
    order = {
        "amount_from": 10,
	    "amount_to": 20,
	    "deadline": datetime(2020, 5, 24).strftime("%Y-%m-%dT%H:%M:%SZ"),
	    "sender": tezos.addresses[0],
	    "token_from": ZERO_ADDRESS,
	    "token_to": tezos.addresses[2],
    }
    # Should fail when making an order from Tz and not sending the proper amount
    _test_error_msg(lambda: tezos.wait(e.dvp.ci.makeOrder(order)), "63")
    _test_error_msg(lambda: tezos.wait(e.dvp.ci.makeOrder(order).with_amount(Decimal(order['amount_from'] - 1))), "63")
    _test_error_msg(lambda: tezos.wait(e.dvp.ci.makeOrder(order).with_amount(Decimal(order['amount_from'] + 1))), "63")
    # Should work
    resp = tezos.wait(e.dvp.ci.makeOrder(order).with_amount(Decimal(order['amount_from'])))  # tez
    assert resp['contents'][0]['amount'] == str(int(float(Decimal(10)) * 1e6))  # mutez
    assert e.dvp.ci.storage()['counter'] == init_order_counter + 2

    # Token to Tz
    order = {
        "amount_from": 10,
	    "amount_to": 20,
	    "deadline": datetime(2020, 5, 24).strftime("%Y-%m-%dT%H:%M:%SZ"),
	    "sender": tezos.addresses[0],
	    "token_from": tezos.addresses[1],
	    "token_to": ZERO_ADDRESS,
    }
    # Should work
    tezos.wait(e.dvp.ci.makeOrder(order))

    addr_to_contract_type_no_tokens = lambda e: {'dvp': e.dvp.addr}
    gas_ops.append(map_contract_addr_to_type(e, addr_to_contract_type_no_tokens))


def test_take_order_mini(ligo, tezos):
    owner, alice, bob, random_investor = [tezos.addresses[i] for i in range(4)]
    owner_client, alice_client, bob_client, random_investor_client = [tezos.clients[i] for i in range(4)]
    init_tokens = 100

    params = EnvParams(DvP=DvPParams(owner=tezos.addresses[0]))
    e = MiniEnv(ligo, tezos, params)
    e.setup_env(alice, bob, init_tokens, init_tokens)

    order = {
        "amount_from": 10,
        "amount_to": 20,
        "deadline": datetime(2021, 5, 24).strftime("%Y-%m-%dT%H:%M:%SZ"),
        "sender": alice,
        "token_from": e.tokenA.addr,
        "token_to": e.tokenB.addr,
    }

    def setup_order():
        resp = tezos.wait(e.dvp.ci.makeOrder(order))
        order_id = resp['contents'][0]['metadata']['operation_result']['big_map_diff'][0]['key']['int']

        alice_transfer_from_params = e.tokenA.make_set_allow_transfer_from_params(
            authority=e.dvp.addr,
            transfer_for=alice,
            amount=order["amount_from"],
        )
        e.tokenA.switch_key(alice_client)
        e.tezos.wait(e.tokenA.ci.setAllowTransferFrom(alice_transfer_from_params))
        bob_transfer_from_params = e.tokenA.make_set_allow_transfer_from_params(
            authority=e.dvp.addr,
            transfer_for=bob,
            amount=order["amount_to"],
        )
        e.tokenB.switch_key(bob_client)
        e.tezos.wait(e.tokenB.ci.setAllowTransferFrom(bob_transfer_from_params))
        return order_id

    order_id = setup_order()

    # Random investor cannot take order on behalf of bob
    cb = lambda: tezos.wait(e.dvp.ci.takeOrder({"order_id": order_id, "take_for": bob}))
    e.dvp.switch_key(random_investor_client)
    _test_error_msg(cb, "64")

    # Bob can take the order
    e.dvp.switch_key(bob_client)
    tezos.wait(e.dvp.ci.takeOrder({"order_id": order_id, "take_for": bob}))

    assert e.tokenA.ci.big_map_get('balances/' + alice) == init_tokens - order["amount_from"]
    assert e.tokenA.ci.big_map_get('balances/' + bob) == order["amount_from"]
    assert e.tokenB.ci.big_map_get('balances/' + alice) == order["amount_to"]
    assert e.tokenB.ci.big_map_get('balances/' + bob) == init_tokens - order["amount_to"]

    # admin can take order
    order_id = setup_order()
    e.dvp.switch_key(owner_client)
    tezos.wait(e.dvp.ci.takeOrder({"order_id": order_id, "take_for": bob}))

    # Order does not exist
    cb = lambda: tezos.wait(e.dvp.ci.takeOrder({"order_id": order_id, "take_for": bob}))
    _test_error_msg(cb, "61")

    gas_ops.append(map_contract_addr_to_type(e, addr_to_contract_type_mini))


def addr_to_contract_type_mini_token_to_tz(env):
    return {
        env.tokenA.addr: 'mini_nyx',
        env.tokenB.addr: 'mini_nyx',
        env.dvp.addr: 'dvp_mini_token_to_tz',
    }


def test_take_order_mini_token_to_tz(ligo, tezos):
    alice, bob = [tezos.addresses[i] for i in range(2)]
    alice_client, bob_client = [tezos.clients[i] for i in range(2)]
    init_tokens = 100

    params = EnvParams(DvP=DvPParams(owner=tezos.addresses[0]))
    e = MiniEnv(ligo, tezos, params)
    e.setup_env(alice, bob, init_tokens, init_tokens)

    order = {
        "amount_from": 10,
	    "amount_to": 20,
	    "deadline": datetime(2021, 5, 24).strftime("%Y-%m-%dT%H:%M:%SZ"),
	    "sender": alice,
	    "token_from": e.tokenA.addr,
	    "token_to": ZERO_ADDRESS,
    }
    resp = tezos.wait(e.dvp.ci.makeOrder(order))
    order_id = resp['contents'][0]['metadata']['operation_result']['big_map_diff'][0]['key']['int']

    alice_transfer_from_params = e.tokenA.make_set_allow_transfer_from_params(
        authority=e.dvp.addr,
        transfer_for=alice,
        amount=order["amount_from"],
    )
    e.tokenA.switch_key(alice_client)
    e.tezos.wait(e.tokenA.ci.setAllowTransferFrom(alice_transfer_from_params))

    e.dvp.switch_key(bob_client)
    resp = tezos.wait(e.dvp.ci.takeOrder({"order_id": order_id, "take_for": bob}).with_amount(Decimal(order['amount_to'])))

    # Check that bob got his tokens from alice
    assert e.tokenA.ci.big_map_get('balances/' + alice) == init_tokens - order["amount_from"]
    assert e.tokenA.ci.big_map_get('balances/' + bob) == order["amount_from"]

    # Check that bob sent the proper amount to the dvp contract which itself
    # forwarded it to alice
    mutez_amount = str(int(order['amount_to'] * 1e6))
    assert resp['contents'][0]['metadata']['internal_operation_results'][1]['amount'] == mutez_amount
    assert resp['contents'][0]['metadata']['internal_operation_results'][1]['result']['balance_updates'][0]['contract'] == e.dvp.addr
    assert resp['contents'][0]['metadata']['internal_operation_results'][1]['result']['balance_updates'][0]['change'] == '-' + mutez_amount
    assert resp['contents'][0]['metadata']['internal_operation_results'][1]['result']['balance_updates'][1]['contract'] == alice
    assert resp['contents'][0]['metadata']['internal_operation_results'][1]['result']['balance_updates'][1]['change'] == mutez_amount

    assert resp['contents'][0]['amount'] == mutez_amount
    assert resp['contents'][0]['metadata']['operation_result']['balance_updates'][0]['contract'] == bob
    assert resp['contents'][0]['metadata']['operation_result']['balance_updates'][0]['contract'] == bob
    assert resp['contents'][0]['metadata']['operation_result']['balance_updates'][0]['change'] == '-' + mutez_amount
    assert resp['contents'][0]['metadata']['operation_result']['balance_updates'][1]['contract'] == e.dvp.addr
    assert resp['contents'][0]['metadata']['operation_result']['balance_updates'][1]['change'] == mutez_amount

    gas_ops.append(map_contract_addr_to_type(e, addr_to_contract_type_mini_token_to_tz))


def addr_to_contract_type_tz_to_mini_token(env):
    return {
        env.tokenA.addr: 'mini_nyx',
        env.tokenB.addr: 'mini_nyx',
        env.dvp.addr: 'dvp_tz_to_mini_token',
    }

def test_take_order_tz_to_mini_token(ligo, tezos):
    alice, bob = [tezos.addresses[i] for i in range(2)]
    alice_client, bob_client = [tezos.clients[i] for i in range(2)]
    init_tokens = 100

    params = EnvParams(DvP=DvPParams(owner=tezos.addresses[0]))
    e = MiniEnv(ligo, tezos, params)
    e.setup_env(alice, bob, init_tokens, init_tokens)

    order = {
        "amount_from": 10,
	    "amount_to": 20,
	    "deadline": datetime(2021, 5, 24).strftime("%Y-%m-%dT%H:%M:%SZ"),
	    "sender": alice,
	    "token_from": ZERO_ADDRESS,
	    "token_to": e.tokenB.addr,
    }
    resp = tezos.wait(e.dvp.ci.makeOrder(order).with_amount(Decimal(order['amount_from'])))
    order_id = resp['contents'][0]['metadata']['operation_result']['big_map_diff'][0]['key']['int']

    bob_transfer_from_params = e.tokenB.make_set_allow_transfer_from_params(
        authority=e.dvp.addr,
        transfer_for=bob,
        amount=order["amount_to"],
    )
    e.tokenB.switch_key(bob_client)
    e.tezos.wait(e.tokenB.ci.setAllowTransferFrom(bob_transfer_from_params))

    e.dvp.switch_key(bob_client)
    resp = tezos.wait(e.dvp.ci.takeOrder({"order_id": order_id, "take_for": bob}))

    # Check that bob got his tokens from alice
    assert e.tokenB.ci.big_map_get('balances/' + bob) == init_tokens - order["amount_to"]
    assert e.tokenB.ci.big_map_get('balances/' + alice) == order["amount_to"]

    # Check that the dvp contract sent the proper amount to alice
    mutez_amount = str(int(order['amount_from'] * 1e6))
    assert resp['contents'][0]['metadata']['internal_operation_results'][0]['amount'] == mutez_amount
    assert resp['contents'][0]['metadata']['internal_operation_results'][0]['result']['balance_updates'][0]['contract'] == e.dvp.addr
    assert resp['contents'][0]['metadata']['internal_operation_results'][0]['result']['balance_updates'][0]['change'] == '-' + mutez_amount
    assert resp['contents'][0]['metadata']['internal_operation_results'][0]['result']['balance_updates'][1]['contract'] == bob
    assert resp['contents'][0]['metadata']['internal_operation_results'][0]['result']['balance_updates'][1]['change'] == mutez_amount

    gas_ops.append(map_contract_addr_to_type(e, addr_to_contract_type_tz_to_mini_token))


def addr_to_contract_type_half_mini(env):
    return {
        env.tokenA_env.kyc.addr: 'kyc',
        env.tokenA_env.issuer.addr: 'issuer',
        env.tokenA_env.token.addr: 'token',
        env.tokenB.addr: 'mini_nyx',
        env.dvp.addr: 'half_mini_dvp',
    }


class HalfMiniEnv(DvPEnv):
    def setup_env(self, alice, bob, bob_amount, alice_amount):
        owner = self.dvp.ci.storage()['owner']

        token_params = TokenParams(owner=owner)#, standalone=True)
        env_params = EnvParams(Token=token_params)
        self.tokenA_env = TokenEnv(self.ligo, self.tezos, env_params).setup_env([alice, bob], mint=False)

        token_params = MiniNyxParams(owner=owner)
        self.tokenB = MiniNyx(self.ligo, self.tezos, token_params)

        self.tezos.wait(self.tokenA_env.token.ci.mint({'amount': alice_amount, 'tr_to': alice}))
        self.tezos.wait(self.tokenB.ci.mint({'amount': bob_amount, 'tr_to': bob}))

def test_take_order_half_mini(ligo, tezos):
    alice, bob = [tezos.addresses[i] for i in range(2)]
    alice_client, bob_client = [tezos.clients[i] for i in range(2)]
    init_tokens = 100

    params = EnvParams(DvP=DvPParams(owner=tezos.addresses[0]))
    e = HalfMiniEnv(ligo, tezos, params)
    e.setup_env(alice, bob, init_tokens, init_tokens)

    order = {
        "amount_from": 10,
	    "amount_to": 20,
	    "deadline": datetime(2021, 5, 24).strftime("%Y-%m-%dT%H:%M:%SZ"),
	    "sender": alice,
	    "token_from": e.tokenA_env.token.addr,
	    "token_to": e.tokenB.addr,
    }
    resp = tezos.wait(e.dvp.ci.makeOrder(order))
    order_id = resp['contents'][0]['metadata']['operation_result']['big_map_diff'][0]['key']['int']
    tezos.wait(e.dvp.ci.makeOrder(order))
    tezos.wait(e.dvp.ci.makeOrder(order))
    tezos.wait(e.dvp.ci.makeOrder(order))
    tezos.wait(e.dvp.ci.makeOrder(order))
    tezos.wait(e.dvp.ci.makeOrder(order))
    tezos.wait(e.dvp.ci.makeOrder(order))
    tezos.wait(e.dvp.ci.makeOrder(order))

    alice_transfer_from_params = e.tokenA_env.token.make_set_allow_transfer_from_params(
        authority=e.dvp.addr,
        transfer_for=alice,
        amount=order["amount_from"],
    )
    e.tokenA_env.token.switch_key(alice_client)
    e.tezos.wait(e.tokenA_env.token.ci.setAllowTransferFrom(alice_transfer_from_params))
    bob_transfer_from_params = e.tokenA_env.token.make_set_allow_transfer_from_params(
        authority=e.dvp.addr,
        transfer_for=bob,
        amount=order["amount_to"],
    )
    e.tokenB.switch_key(bob_client)
    e.tezos.wait(e.tokenB.ci.setAllowTransferFrom(bob_transfer_from_params))

    e.dvp.switch_key(bob_client)
    tezos.wait(e.dvp.ci.takeOrder({"order_id": order_id, "take_for": bob}))

    assert e.tokenA_env.token.ci.big_map_get('balances/' + alice) == init_tokens - order["amount_from"]
    assert e.tokenA_env.token.ci.big_map_get('balances/' + bob) == order["amount_from"]
    assert e.tokenB.ci.big_map_get('balances/' + alice) == order["amount_to"]
    assert e.tokenB.ci.big_map_get('balances/' + bob) == init_tokens - order["amount_to"]

    gas_ops.append(map_contract_addr_to_type(e, addr_to_contract_type_half_mini))


# Nyx tokens

def addr_to_contract_type(env):
    return {
        env.tokenA_env.kyc.addr: 'kyc',
        env.tokenA_env.issuer.addr: 'issuer',
        env.tokenA_env.token.addr: 'token',
        env.tokenB_env.kyc.addr: 'kyc',
        env.tokenB_env.issuer.addr: 'issuer',
        env.tokenB_env.token.addr: 'token',
        env.dvp.addr: 'dvp',
    }

class Env(DvPEnv):
    def setup_env(self, alice, bob, bob_amount, alice_amount):
        def get_token(invs):
            owner = self.tezos.addresses[0]
            token_params = TokenParams(owner=owner)#, standalone=True)
            env_params = EnvParams(Token=token_params)
            return TokenEnv(self.ligo, self.tezos, env_params).setup_env(invs, mint=False)

        self.tokenA_env = get_token([alice, bob])
        self.tokenB_env = get_token([bob, alice])

        self.tezos.wait(self.tokenA_env.token.ci.mint({'amount': alice_amount, 'tr_to': alice}))
        self.tezos.wait(self.tokenB_env.token.ci.mint({'amount': bob_amount, 'tr_to': bob}))

def skip_test_take_order(ligo, tezos):
    alice, bob = [tezos.addresses[i] for i in range(2)]
    alice_client, bob_client = [tezos.clients[i] for i in range(2)]
    init_tokens = 200

    params = EnvParams(DvP=DvPParams(owner=tezos.addresses[0]))
    e = Env(ligo, tezos, params)
    e.setup_env(alice, bob, init_tokens, init_tokens)

    order = {
        "amount_from": 10,
	    "amount_to": 20,
	    "deadline": datetime(2021, 5, 24).strftime("%Y-%m-%dT%H:%M:%SZ"),
	    "sender": alice,
	    "token_from": e.tokenA_env.token.addr,
	    "token_to": e.tokenB_env.token.addr,
    }
    resp = tezos.wait(e.dvp.ci.makeOrder(order))
    order_id = resp['contents'][0]['metadata']['operation_result']['big_map_diff'][0]['key']['int']

    alice_transfer_from_params = e.tokenA_env.token.make_set_allow_transfer_from_params(
        authority=e.dvp.addr,
        transfer_for=alice,
        amount=order["amount_from"],
    )
    e.tokenA_env.token.switch_key(alice_client)
    e.tezos.wait(e.tokenA_env.token.ci.setAllowTransferFrom(alice_transfer_from_params))
    bob_transfer_from_params = e.tokenA_env.token.make_set_allow_transfer_from_params(
        authority=e.dvp.addr,
        transfer_for=bob,
        amount=order["amount_to"],
    )
    e.tokenB_env.token.switch_key(bob_client)
    e.tezos.wait(e.tokenB_env.token.ci.setAllowTransferFrom(bob_transfer_from_params))

    e.dvp.switch_key(bob_client)
    tezos.wait(e.dvp.ci.takeOrder({"order_id": order_id, "take_for": bob}))

    assert e.tokenA_env.token.ci.big_map_get('balances/' + alice) == init_tokens - order["amount_from"]
    assert e.tokenA_env.token.ci.big_map_get('balances/' + bob) == order["amount_from"]
    assert e.tokenB_env.token.ci.big_map_get('balances/' + alice) == order["amount_to"]
    assert e.tokenB_env.token.ci.big_map_get('balances/' + bob) == init_tokens - order["amount_to"]

    cb = lambda: tezos.wait(e.dvp.ci.takeOrder({"order_id": order_id, "take_for": bob}))
    _test_error_msg(cb, "61")

    gas_ops.append(map_contract_addr_to_type(e, addr_to_contract_type))

def test_save_gas_costs():
    import json
    with open('gas/dvp.json', 'w', encoding='utf-8') as f:
        json.dump(gas_ops, f, ensure_ascii=False, indent=4)
