#+begin_src python :session mini_nyx :exports none
import sys ; sys.path.append("..")
import os ; os.chdir("..")
#+end_src

#+RESULTS:

#+BEGIN_SRC ditaa :file images/mini-nyx.png :exports results
+--------------+            +--------------+
|              |            |              |
|   MiniNyx    +- - - - - ->+  Event Sink  |
|              |            |              |
+--------------+            +--------------+
#+END_SRC

#+RESULTS:
[[file:images/mini-nyx.png]]

Mini Nyx has the same API as a Nyx security token, but does not check investors with the issuing entity. It is therefore very lightweight. Since this is a standalone contract, no need to setup a Pynyx environment, we can simply deploy the contracts

#+begin_src python :session mini_nyx :results output :exports both
from pytest_ligo import Ligo
from pytest_tezos import Tezos
from pynyx.contracts import MiniNyxParams, MiniNyx
from pynyx.helpers import LOG

tezos = Tezos()
ligo = Ligo()

owner = tezos.addresses[0]
params = MiniNyxParams(owner)
mini_nyx = MiniNyx(ligo, tezos, params)
LOG(mini_nyx.ci.storage())
#+end_src

#+RESULTS:
: { 'allow_transfer_from': 255,
:   'balances': 256,
:   'name': 'Token',
:   'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx',
:   'symbol': 'TK',
:   'tokens': 100,
:   'total_supply': 0}

Where the ~balances~ key is a big map id.

We can therefore start minting tokens to addresses skipping the transfer checking procedures with the issuer and KYC registrar as on the Nyx security token:

#+begin_src python :session mini_nyx :results output :exports both
inv_from, inv_to, amount = tezos.addresses[0], tezos.addresses[2], 10

tezos.wait(mini_nyx.ci.mint({"tr_to": inv_from, "amount": amount}))
assert mini_nyx.ci.big_map_get(f'balances/{inv_from}') == amount
#+end_src

#+RESULTS:

Let's now start transferring some tokens. Notice the use of a list in the argument to `transfer`, this allows for batch transferring:

#+begin_src python :session mini_nyx :results output :exports both
tezos.wait(mini_nyx.ci.transfer([{"tr_to": inv_to, "amount": amount}]))
assert mini_nyx.ci.big_map_get(f'balances/{inv_from}') == 0
assert mini_nyx.ci.big_map_get(f'balances/{inv_to}') == amount
#+end_src

#+RESULTS:

We can also authorize and address to transfer on behalf of another one. Note the key ~prev_amount~ int the ~set_allow_transfer_from_params~ dictionary, that prevents the following [[https://docs.google.com/document/d/1YLPtQxZu1UAvO9cZ1O2RPXBbT0mooh4DYKjA_jp-RLM/edit#][attack]].

#+begin_src python :session mini_nyx :results output :exports both
authority_index = 4
authority = tezos.addresses[authority_index]
inv = tezos.addresses[2]

transfer_from_info = {
    'transfer_for': inv,
    'amount': 5,
}
set_allow_transfer_from_params = {
    'authority': authority,
    'prev_amount': 0,
    'transfer_from_info': transfer_from_info,
    'remove': False
}
LOG(mini_nyx.ci.big_map_get(f'allow_transfer_from/{authority}'), f"Amount of tokens {authority} can transfer on behalf of {inv}")
#+end_src

#+RESULTS:
: === Amount of tokens tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv can transfer on behalf of tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU ===
: 
: {'amount': 5, 'transfer_for': 'tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU'}


#+begin_src python :session mini_nyx :results output :exports both
authority_client = tezos.clients[authority_index]
mini_nyx.ci.transferFrom.key = authority_client.key

tr_to = tezos.addresses[0]
LOG(mini_nyx.ci.big_map_get(f'allow_transfer_from/{authority}'), "Allowances before")
LOG(mini_nyx.ci.big_map_get(f'balances/{tr_to}'), f"{authority} balance before")
LOG(mini_nyx.ci.big_map_get(f'balances/{inv}'), f"{inv} balance before")
tezos.wait(mini_nyx.ci.transferFrom([{
    'tr_from': inv,
    'tr_to': tr_to,
    'amount': 3,
}]))
LOG(mini_nyx.ci.big_map_get(f'allow_transfer_from/{authority}'), "Allowances after")
LOG(mini_nyx.ci.big_map_get(f'balances/{tr_to}'), f"{authority} balance after")
LOG(mini_nyx.ci.big_map_get(f'balances/{inv}'), f"{inv} balance after")
#+end_src

#+RESULTS:
#+begin_example
=== Allowances before ===

{'amount': 5, 'transfer_for': 'tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU'}


=== tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv balance before ===

0


=== tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU balance before ===

10


=== Allowances after ===

{'amount': 2, 'transfer_for': 'tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU'}


=== tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv balance after ===

3


=== tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU balance after ===

7
#+end_example
