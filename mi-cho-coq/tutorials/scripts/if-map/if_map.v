Require Import Michocoq.macros.
Import Michocoq.syntax.
Require Import Michocoq.semantics.
Require Import Michocoq.util.
Import error.
Require Import ZArith.
Import comparable.

Definition amount_ty := nat.
Definition destination_address_ty := address.
Definition parameter_ty := (pair amount_ty destination_address_ty).

Definition balances_ty := map address nat.
Definition owner_ty := address.
Definition supply_ty := nat.
Definition storage_ty := (pair (pair balances_ty owner_ty) supply_ty).

Module ST : (SelfType with Definition self_type := parameter_ty).
  Definition self_type := parameter_ty.
End ST.

Module if_contract(C:ContractContext)(E:Env ST C).

Module semantics := Semantics ST C E. Import semantics.

Import List.ListNotations.



Definition DROP2 {a b SA self_type} : instruction self_type Datatypes.false (a ::: b ::: SA) SA :=
  DROP (A := [_; _]) 2 eq_refl.
Definition DROP3 {a b c SA self_type} : instruction self_type Datatypes.false (a ::: b ::: c ::: SA) SA :=
  DROP (A := [_; _; _]) 3 eq_refl.
Definition DROP4 {a b c d SA self_type} : instruction self_type Datatypes.false (a ::: b ::: c ::: d ::: SA) SA :=
  DROP (A := [_; _; _; _]) 4 eq_refl.
Definition DROP5 {a b c d e SA self_type} : instruction self_type Datatypes.false (a ::: b ::: c ::: d ::: e ::: SA) SA :=
  DROP (A := [_; _; _; _; _]) 5 eq_refl.



Definition DIG1 {a S2 t self_type} : instruction self_type Datatypes.false (a ::: (t ::: S2)) (t ::: a ::: S2) :=
  DIG (S1 := [_]) 1 eq_refl.
Definition DIG2 {a b S2 t self_type} :
  instruction self_type _ (a ::: b :::(t ::: S2)) (t ::: a ::: b ::: S2) :=
  DIG (S1 := [_; _]) 2 eq_refl.
Definition DIG3 {a b c S2 t self_type} :
  instruction self_type _ (a ::: b ::: c ::: (t ::: S2)) (t ::: a ::: b ::: c ::: S2) :=
  DIG (S1 := [_; _; _]) 3 eq_refl.
Definition DIG4 {a b c d S2 t self_type} :
  instruction self_type _ (a ::: b ::: c ::: d ::: (t ::: S2)) (t ::: a ::: b ::: c ::: d ::: S2) :=
  DIG (S1 := [_; _; _; _]) 4 eq_refl.
Definition DIG5 {a b c d e S2 t self_type} :
  instruction self_type _ (a ::: b ::: c ::: d ::: e ::: (t ::: S2)) (t ::: a ::: b ::: c ::: d ::: e ::: S2) :=
  DIG (S1 := [_; _; _; _; _]) 5 eq_refl.
Definition DIG6 {a b c d e f S2 t self_type} :
  instruction self_type _ (a ::: b ::: c ::: d ::: e ::: f ::: (t ::: S2)) (t ::: a ::: b ::: c ::: d ::: e ::: f ::: S2) :=
  DIG (S1 := [_; _; _; _; _; _]) 6 eq_refl.

Definition DUG1 {a S2 t self_type} : instruction self_type Datatypes.false (t ::: a ::: S2) (a ::: (t ::: S2)) :=
  DUG (S1 := [_]) 1 eq_refl.
Definition DUG2 {a b S2 t self_type} :
  instruction self_type _ (t ::: a ::: b ::: S2) (a ::: b :::(t ::: S2)) :=
  DUG (S1 := [_; _]) 2 eq_refl.
Definition DUG3 {a b c S2 t self_type} :
  instruction self_type _ (t ::: a ::: b ::: c ::: S2) (a ::: b ::: c ::: (t ::: S2)) :=
  DUG (S1 := [_; _; _]) 3 eq_refl.
Definition DUG4 {a b c d S2 t self_type} :
  instruction self_type _ (t ::: a ::: b ::: c ::: d ::: S2) (a ::: b ::: c ::: d ::: (t ::: S2)) :=
  DUG (S1 := [_; _; _; _]) 4 eq_refl.
Definition DUG5 {a b c d e S2 t self_type} :
  instruction self_type _ (t ::: a ::: b ::: c ::: d ::: e ::: S2) (a ::: b ::: c ::: d ::: e ::: (t ::: S2)) :=
  DUG (S1 := [_; _; _; _; _]) 5 eq_refl.
Definition DUG6 {a b c d e f S2 t self_type} :
  instruction self_type _ (t ::: a ::: b ::: c ::: d ::: e ::: f ::: S2) (a ::: b ::: c ::: d ::: e ::: f ::: (t ::: S2)) :=
  DUG (S1 := [_; _; _; _; _; _]) 6 eq_refl.

Definition ADD_nat {S} : instruction (Some ST.self_type) _ (nat ::: nat ::: S) (nat ::: S) := ADD.




Definition mint_contract : full_contract _ ST.self_type storage_ty :=
       ( DUP ;;
         CAR ;;
         DIG1 ;;
         DUP ;;
         DUG2 ;;
         CDR ;;
         DUP ;;
         CAR ;;
         CDR ;;
         SENDER ;;
         COMPARE ;;
         NEQ ;;
         IF ( FAIL ) ( UNIT ) ;;
         DIG1 ;;
         DUP ;;
         DUG2 ;;
         CAR ;;
         CAR ;;
         DIG3 ;;
         DUP ;;
         DUG4 ;;
         CDR ;;
         GET (i := get_map address nat);;
         IF_NONE
           ( DIG1 ;;
             DUP ;;
             DUG2 ;;
             CAR ;;
             CAR ;;
             DIG3 ;;
             DUP ;;
             DUG4 ;;
             CAR ;;
             SOME ;;
             DIG4 ;;
             DUP ;;
             DUG5 ;;
             CDR ;;
             UPDATE (i := Mk_update address (option nat) (map address nat) (Update_variant_map address nat)))
           ( DIG2 ;;
             DUP ;;
             DUG3 ;;
             CAR ;;
             CAR ;;
             DIG4 ;;
             DUP ;;
             DUG5 ;;
             CAR ;;
             DIG2 ;;
             DUP ;;
             DUG3 ;;
             ADD ;;
             SOME ;;
             DIG5 ;;
             DUP ;;
             DUG6 ;;
             CDR ;;
             UPDATE (i := Mk_update address (option nat) (map address nat) (Update_variant_map address nat));;
             DIP1 ( DROP1 ) ) ;;
         DIG2 ;;
         DUP ;;
         DUG3 ;;

         CDR ;;
	       DIG3 ;;
	       DUP ;;
	       DUG4 ;;
	       CAR ;;
	       CDR ;;
	       DIG2 ;;
	       DUP ;;
	       DUG3 ;;
	       PAIR ;;
	       PAIR ;;

         NIL operation ;;
         PAIR ;;
         DIP1 ( DROP5 ) ).

Definition amount_ty := nat.
Definition destination_address_ty := address.
Definition parameter_ty := (pair amount_ty destination_address_ty).


Lemma address_compare_Eq : forall a : address_constant,
  address_compare a a = Eq.
Proof.
  intros a. induction a. simpl. rewrite string_compare_Eq_correct. reflexivity.
Qed.

Lemma sender_is_not_owner : forall owner sender: address_constant,
    negb
      (comparison_to_int
         (address_compare
            owner sender) =? 0) %Z = true
    -> owner <> sender.
Proof.
  intros owner sender.
  rewrite Bool.negb_true_iff.
  rewrite Z.eqb_neq.
  unfold comparison_to_int.
  intuition.
  destruct H.
  rewrite H0. rewrite address_compare_Eq. reflexivity.
Qed.


Lemma sender_is_owner : forall owner sender: address_constant,
    negb
      (comparison_to_int
         (address_compare
            owner sender) =? 0) %Z = false
    -> owner = sender.
Proof.
  intros owner sender.
  rewrite Bool.negb_false_iff.
  rewrite (eqb_eq address).
  intuition.
Qed.

Lemma mint_contract_correct :
  forall (amount : data amount_ty)
         (destination_address : data destination_address_ty)
         (balances_in : data balances_ty)
         (owner : data owner_ty)
         (supply_in : data supply_ty)
         (balances_out : data balances_ty)
         (supply_out : data supply_ty)
         (ops : data (list operation))
         (fuel : Datatypes.nat),
  let input : data parameter_ty := (amount, destination_address) in
  let storage_in : data storage_ty := ((balances_in, owner), supply_in) in
  let storage_out : data storage_ty := ((balances_out, owner), supply_out) in
  fuel >= 100 ->
  eval env mint_contract fuel ((input, storage_in), tt) = Return ((ops, storage_out), tt)
  <->
  (sender env) = owner /\
  match (get _ _ _ (Get_variant_map _ nat) destination_address balances_in) with
  | Some n1 => match (get _ _ _ (Get_variant_map _ nat) destination_address balances_out) with
              | Some n2 => n2 = N.add n1 amount
              | None => False
              end
  | None => match (get _ _ _ (Get_variant_map _ nat) destination_address balances_out) with
              | Some n2 => n2 = amount
              | None => False
              end 
  end.
Proof.
  intros amount destination_address balances_in owner supply_in balances_out supply_out ops fuel input storage_in storage_out Hfuel.
  unfold eval.
  rewrite return_precond.
  rewrite eval_precond_correct.
  unfold ">=" in Hfuel.
  simpl.
  do 14 more_fuel ; simpl.
  destruct (negb (comparison_to_int (address_compare (sender env) owner) =? 0)%Z) eqn:condition.
  (* owner is not sender *)
  - do 2 more_fuel ; simpl. split.
    + (* -> *) intros H. contradiction H.
    + (* <- *) intros H.
      apply sender_is_not_owner in condition.
      intuition.
  (* owner is sender *)
  - do 11 more_fuel ; simpl. split.
    + destruct (map.get address_constant N address_compare destination_address balances_in) eqn:mapget.
      (* key is in the map *)
      * do 21 more_fuel ; simpl. intros. inversion H.
        split.
        -- apply sender_is_owner in condition. trivial.
        -- 

        apply map.map_getmem in mapget. Search map.mem.
        intros H. split.
        -- apply sender_is_owner in condition. intuition.
        -- split.
          ++ congruence.
          ++ destruct (map.get address_constant N address_compare destination_address balances_out) eqn:mapget2.
             ** apply map.map_getmem in mapget2.
        admit.
      * admit.
    + destruct (map.get address_constant N address_compare destination_address balances_in) eqn:mapget.
      (* key is not in the map *)
      * admit.
      * admit.
(* apply sender_is_owner in condition. *)
Admitted.
