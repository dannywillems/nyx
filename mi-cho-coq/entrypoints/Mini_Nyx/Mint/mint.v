Require Import Michocoq.macros.
Import Michocoq.syntax.
Import comparable.
Require Import NArith.
Require Import Michocoq.semantics.
Require Import Michocoq.util.
Import error.
Require Import ZArith.
Require Michocoq.map.

Definition parameter_ty := (pair (nat) (address)).
Definition storage_ty := (pair (pair (pair (big_map (Cpair (address) (address)) nat)
                      (big_map address nat))
                (pair (string) (address)))
          (pair (pair (string) (nat)) (nat))).


Module ST : (SelfType with Definition self_type := parameter_ty).
  Definition self_type := parameter_ty.
End ST.

Module mint(C:ContractContext)(E:Env ST C).

Module semantics := Semantics ST C E. Import semantics.

Import List.ListNotations.

Definition mint : full_contract _ ST.self_type storage_ty :=
( NIL operation ;;
         DIG (S1 := [_]) 1 eq_refl ;;
         DUP ;;
         DUG (S1 := [_; _]) 2 eq_refl ;;
         CAR ;;
         DIG (S1 := [_; _]) 2 eq_refl ;;
         DUP ;;
         DUG (S1 := [_; _; _]) 3 eq_refl ;;
         CDR ;;
         DUP ;;
         CAR ;;
         CDR ;;
         CDR ;;
         SENDER ;;
         COMPARE ;;
         NEQ ;;
         IF ( FAILWITH ) ( UNIT ) ;;
         DIG (S1 := [_]) 1 eq_refl;;
         DUP ;;
         DUG (S1 := [_; _]) 2 eq_refl ;;
         CDR ;;
         CAR ;;
         CDR ;;
         DIG (S1 := [_; _; _]) 3 eq_refl ;;
         DUP ;;
         DUG (S1 := [_; _; _; _]) 4 eq_refl ;;
         CAR ;;
         COMPARE ;;
         GT ;;
         IF ( FAILWITH)
            ( DIG (S1 := [_; _]) 2 eq_refl ;;
              DUP ;;
              DUG (S1 := [_; _; _]) 3 eq_refl ;;
              CAR ;;
              DIG (S1 := [_; _]) 2 eq_refl ;;
              DUP ;;
              DUG (S1 := [_; _; _]) 3 eq_refl ;;
              CDR ;;
              CAR ;;
              CDR ;;
              SUB ;;
              ABS ) ;;
         DIG (S1 := [_; _]) 2 eq_refl ;;
         DUP ;;
         DUG (S1 := [_; _; _]) 3 eq_refl ;;
         CAR ;;
         CAR ;;
         CDR ;;
         DIG (S1 := [_; _; _; _]) 4 eq_refl ;;
         DUP ;;
         DUG (S1 := [_; _; _; _; _]) 5 eq_refl ;;
         CDR ;;
         GET (i := get_bigmap _ _) ;;
         IF_NONE
           ( DIG (S1 := [_; _]) 2 eq_refl ;;
             DUP ;;
             DUG (S1 := [_; _; _]) 3 eq_refl ;;
             CAR ;;
             CAR ;;
             CDR ;;
             DIG (S1 := [_; _; _; _]) 4 eq_refl ;;
             DUP ;;
             DUG (S1 := [_; _; _; _; _]) 5 eq_refl ;;
             CAR ;;
             SOME ;;
             DIG (S1 := [_; _; _; _; _]) 5 eq_refl ;;
             DUP ;;
             DUG (S1 := [_; _; _; _; _; _]) 6 eq_refl ;;
             CDR ;;
             UPDATE (i := update_bigmap _ _))
           ( DIG (S1 := [_; _; _]) 3 eq_refl ;;
             DUP ;;
             DUG (S1 := [_; _; _; _]) 4 eq_refl ;;
             CAR ;;
             CAR ;;
             CDR ;;
             DIG (S1 := [_; _; _; _; _]) 5 eq_refl ;;
             DUP ;;
             DUG (S1 := [_; _; _; _; _; _]) 6 eq_refl ;;
             CAR ;;
             DIG (S1 := [_; _]) 2 eq_refl ;;
             DUP ;;
             DUG (S1 := [_; _; _]) 3 eq_refl ;;
             ADD (s := add_nat_nat) ;;
             SOME ;;
             DIG (S1 := [_; _; _; _; _; _]) 6 eq_refl ;;
             DUP ;;
             DUG (S1 := [_; _; _; _; _; _; _]) 7 eq_refl ;;
             CDR ;;
             UPDATE (i := update_bigmap _ _);;
             DIP1 ( DROP1 ) ) ;;
         DIG (S1 := [_; _; _; _]) 4 eq_refl ;;
         DUP ;;
         DUG (S1 := [_; _; _; _; _]) 5 eq_refl ;;
         CAR ;;
         DIG (S1 := [_; _; _; _]) 4 eq_refl ;;
         DUP ;;
         DUG (S1 := [_; _; _; _; _]) 5 eq_refl ;;
         CDR ;;
         CDR ;;
         ADD ;;
         DIG (S1 := [_; _]) 2 eq_refl ;;
         DUP ;;
         DUG (S1 := [_; _; _]) 3 eq_refl ;;
         DIG (S1 := [_; _; _; _; _]) 5 eq_refl ;;
         DUP ;;
         DUG (S1 := [_; _; _; _; _; _]) 6 eq_refl ;;
         CDR ;;
         CAR ;;
         CAR ;;
         PAIR ;;
         PAIR ;;
         DIG (S1 := [_; _; _; _]) 4 eq_refl ;;
         DUP ;;
         DUG (S1 := [_; _; _; _; _]) 5 eq_refl ;;
         CAR ;;
         CDR ;;
         CDR ;;
         DIG (S1 := [_; _; _; _; _]) 5 eq_refl ;;
         DUP ;;
         DUG (S1 := [_; _; _; _; _; _]) 6 eq_refl ;;
         CAR ;;
         CDR ;;
         CAR ;;
         PAIR ;;
         DIG (S1 := [_; _]) 2 eq_refl ;;
         DUP ;;
         DUG (S1 := [_; _; _]) 3 eq_refl ;;
         DIG (S1 := [_; _; _; _; _; _]) 6 eq_refl ;;
         DUP ;;
         DUG (S1 := [_; _; _; _; _; _; _]) 7 eq_refl ;;
         CAR ;;
         CAR ;;
         CAR ;;
         PAIR ;;
         PAIR ;;
         PAIR ;;
         DUP ;;
         NIL operation ;;
         PAIR ;;
         DIP1 ( DROP (A := [_; _; _; _; _; _; _; _]) 8 eq_refl ) ).


Lemma mint_correct :
  forall (input : data parameter_ty) storage_in
         (ops : data (list operation)) storage_out
         (fuel : Datatypes.nat),
  fuel >= 100 ->
  eval env mint fuel ((input, storage_in), tt) = Return ((ops, storage_out), tt)
  <->
  ((nil , storage_in , tt) = (ops , storage_out , tt )) /\
  ops = nil.
Proof.
  intros input storage_in ops storage_out fuel Hfuel.
  rewrite return_precond.
  unfold eval.
  rewrite eval_precond_correct.
  unfold ">=" in Hfuel.
  simpl.
  do 11 more_fuel ; simpl.
  more_fuel ; simpl.
  more_fuel ; simpl.
  more_fuel ; simpl.
  do 4 more_fuel ; simpl.
  split.
  - destruct storage_in.
    + destruct p.
      * destruct p1.
        ** 
Admitted.
