![](images/token.png)

The Security Token contract is the contract that keeps track of which
investor has which asset. It is also the contract that handles the
transferring of assets between investors by querying the Issuing Entity
contract associated with it for investor rights. The Issuing Entity will
itself query the various KYC Registrars associated with it for investor
restrictions.

Let us now deploy a Security Token contract with the associated
environment that will take care of setting the Issuing Entity and KYC
Registrar contracts for us. Don't be afraid to look at the
implementation of `environments.TokenEnv` to see how the environment is
setup.

``` python
from pytest_ligo import Ligo
from pytest_tezos import Tezos
tezos = Tezos()

from pynyx.environments import TokenEnv, EnvParams
from pynyx.contracts import TokenParams, ContractParams, IssuerParams

owner = tezos.addresses[0]
kyc_params = ContractParams(owner)
issuer_params = IssuerParams(owner=owner, name="IssuerContract", global_invest_limit=1000)

token_params = TokenParams(
    owner=owner,
    tokens=1000,   # init tokens
    symbol="NYX",
)
env_params = EnvParams(
    KYC=kyc_params,
    Issuer=issuer_params,
    Token=token_params,
)

tezos = Tezos()
nyx = TokenEnv(Ligo(), tezos, env_params)
nyx.log_storage()
```

``` example
=== KYC storage ===

{'members': 257, 'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'}


=== Issuer storage ===

{ 'accounts': 258,
  'country_counters': 259,
  'country_restrictions': 260,
  'document_hashes': 261,
  'global_invest_limit': 1000,
  'global_restriction': False,
  'investor_counter': 0,
  'kyc_registrars': {},
  'name': 'IssuerContract',
  'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx',
  'security_tokens': {}}


=== Token storage ===

{ 'agreement_procedure': False,
  'allow_transfer_from': 262,
  'balances': 263,
  'issuer': 'KT1KDDfmoBLJx3KE9sU9ciKeS49Weue65tZg',
  'name': 'Token',
  'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx',
  'symbol': 'NYX',
  'tokens': 1000,
  'total_supply': 0}
```

As you can see, the token contract storage includes two big map:
`balances` and `allow_transfer_from`. Both of them can be queried with
`nyx.token.ci.big_map_get`.

We can now populate our KYC registar as previously done:

``` python
import time
from pynyx.helpers import get_id_for_region_code

COUNTRIES = {
   "FR": get_id_for_region_code("FR-PAC"),
   "EN": get_id_for_region_code("GB-ENG"),
}

RATINGS = {
   "INDIVIDUAL": 1,
   "INSTITUTIONAL": 2,
}

current_timestamp = int(time.time())

members = [
    {
        "address_0": tezos.addresses[0],
        "country": COUNTRIES['FR'],
        "expires": 1245,
        "rating": 10,
        "region": 1,
        "restricted": False,
    },
    {
        "address_0": tezos.addresses[1],
        "country": COUNTRIES['FR'],
        "expires": 1245,
        "rating": 1,
        "region": 1,
        "restricted": False,
    },
    {
        "address_0": tezos.addresses[2],
        "country": COUNTRIES['EN'],
        "expires": 12345,
        "rating": 1,
        "region": 2,
        "restricted": True,
    },
]

tezos.wait(nyx.kyc.ci.addMembers(members))

from pynyx.helpers import LOG
LOG(nyx.kyc.ci.storage())
```

``` example
{'members': 257, 'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'}
```

The environment already hooked the KYC registrar to the issuing entity
for us, but if we would want to do it manually, we would go about it like that:

``` python
LOG(nyx.issuer.ci.storage()['kyc_registrars'], "Before")
tezos.wait(nyx.issuer.ci.setRegistrar([nyx.kyc.addr, False]))
LOG(nyx.issuer.ci.storage()['kyc_registrars'], "After")
```

``` example
=== Before ===

{}


=== After ===

{'KT1Rp22phiErygFwej2dp9WBkYnNZwvhhyAi': False}
```

We could restrict the registrar as follows:

``` python
tezos.wait(nyx.issuer.ci.setRegistrar([nyx.kyc.addr, True]))
LOG(nyx.issuer.ci.storage()['kyc_registrars'], "Set the registrar as restricted")
tezos.wait(nyx.issuer.ci.setRegistrar([nyx.kyc.addr, False]))
LOG(nyx.issuer.ci.storage()['kyc_registrars'], "And unrestrict it")
```

``` example
=== Set the registrar as restricted ===

{'KT1Rp22phiErygFwej2dp9WBkYnNZwvhhyAi': True}


=== And unrestrict it ===

{'KT1Rp22phiErygFwej2dp9WBkYnNZwvhhyAi': False}
```

Let's now add two unrestricted members, so we can have them exchange
token between each other:

``` python
for i in range(2):
  account_params = {
    "address_0": tezos.addresses[i],
    "registrar": nyx.kyc.addr,
    "restricted": False,
  }
  tezos.wait(nyx.issuer.ci.setAccount(account_params))
  LOG(nyx.issuer.ci.big_map_get(f"accounts/{tezos.addresses[i]}"), f"{tezos.addresses[i]} account info")
```

``` example
=== tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx account info ===

{'registrar': 'KT1Rp22phiErygFwej2dp9WBkYnNZwvhhyAi', 'restricted': False}


=== tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN account info ===

{'registrar': 'KT1Rp22phiErygFwej2dp9WBkYnNZwvhhyAi', 'restricted': False}
```

And configure the countries investors are allowed to invest from:

``` python
from pynyx.contracts import Issuer

params = Issuer.get_update_country_restrictions_params(
    COUNTRIES["FR"],
    10,
    1,
    {1: 10, 2: 5},
)
tezos.wait(nyx.issuer.ci.updateCountryRestrictions([params]))
LOG(nyx.issuer.ci.big_map_get(f'country_restrictions/{COUNTRIES["FR"]}'))
```

``` example
{ 'country_invest_limit': 10,
  'min_rating': 1,
  'rating_restrictions': {1: 10, 2: 5}}
```

The environment class already associated a security token contract with
`nyx.issuer` (accessible from `nyx.token`). But we could have done it
manually by associating a security token contract to the issuer:

``` python
tezos.wait(nyx.issuer.ci.addToken(nyx.token.addr))
LOG(nyx.issuer.ci.storage()['security_tokens'], "Add an unrestricted security token to the issuer")
tezos.wait(nyx.issuer.ci.setToken([nyx.token.addr, True]))
LOG(nyx.issuer.ci.storage()['security_tokens'], "And restrict it")
tezos.wait(nyx.issuer.ci.setToken([nyx.token.addr, False]))
LOG(nyx.issuer.ci.storage()['security_tokens'], "Or not")
```

``` example
=== Add an unrestricted security token to the issuer ===

{'KT1UMF1DJQZhkh8A3DCr9VwzHGCqzWLa3a55': False}


=== And restrict it ===

{'KT1UMF1DJQZhkh8A3DCr9VwzHGCqzWLa3a55': True}


=== Or not ===

{'KT1UMF1DJQZhkh8A3DCr9VwzHGCqzWLa3a55': False}
```

We can now start minting some tokens \!

``` python
LOG(nyx.token.ci.storage()['tokens'], "Tokens left to mint")
amount, inv = 10, tezos.addresses[0]
tezos.wait(nyx.token.ci.mint({'amount': amount, 'tr_to': inv}))
LOG(nyx.token.ci.storage()['tokens'], f"Tokens after minting {amount} to {inv}")
LOG(nyx.token.ci.big_map_get(f'balances/{inv}'), f"Balances after minting {amount} to {inv}")
```

``` example
=== Tokens left to mint ===

1000


=== Tokens after minting 10 to tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx ===

990


=== Balances after minting 10 to tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx ===

10
```

We can now start transferring tokens between unrestricted investors
within the country restrictions and investor limits using
`nyx.token.ci.transfer`. Notice the use of a list as the entrypoint
argument, this allows for batch transferring of tokens.

``` python
# We call `nyx.issuer.ci.transfer` from the address we just minted tokens to
investor_client = tezos.clients[0]
nyx.token.ci.transfer.key = investor_client.key

# And transfer tokens from this investor to some other investor
tr_to = tezos.addresses[1]
tezos.wait(nyx.token.ci.transfer([{
    'tr_to': tr_to,
    'amount': 5,
}]))
LOG(nyx.token.ci.big_map_get(f'balances/{inv}'), f"Balances after transfering tokens to {inv}")
LOG(nyx.token.ci.big_map_get(f'balances/{tr_to}'), f"Balances after transfering tokens to {tr_to}")
```

``` example
=== Balances after transfering tokens to tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx ===

5


=== Balances after transfering tokens to tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN ===

5
```

The security token contract owner can allow other addresses to control some
amount of tokens on behalf an investor. We'll show in the next section
how to use `nyx.token.ci.transferFrom` from a crowdsale contract. Here's
a short snippet demonstrating how to use `~transferFrom`. Note the key
`prev_amount` int the `set_allow_transfer_from_params` dictionary, that
prevents the following
[attack](https://docs.google.com/document/d/1YLPtQxZu1UAvO9cZ1O2RPXBbT0mooh4DYKjA_jp-RLM/edit#).

``` python
authority_index = 4
authority = tezos.addresses[authority_index]

transfer_from_info = {
    'transfer_for': inv,
    'amount': 5,
}
set_allow_transfer_from_params = {
    'authority': authority,
    'transfer_from_info': transfer_from_info,
    'prev_amount': 0,
    'remove': False
}
tezos.wait(nyx.token.ci.setAllowTransferFrom(set_allow_transfer_from_params))
LOG(nyx.token.ci.big_map_get(f'allow_transfer_from/{authority}'), f"Amount of tokens {authority} can transfer on behalf of {inv}")
```

``` example
=== Amount of tokens tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv can transfer on behalf of tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx ===

{'amount': 5, 'transfer_for': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'}
```

We will now call `transfer_from` with the authority's key to send tokens
from `inv` to `tr_to`:

``` python
authority_client = tezos.clients[authority_index]
nyx.token.ci.transferFrom.key = authority_client.key

LOG(nyx.token.ci.big_map_get(f'allow_transfer_from/{authority}'), "Allowances before")
LOG(nyx.token.ci.big_map_get(f'balances/{tr_to}'), f"{authority} balance before")
LOG(nyx.token.ci.big_map_get(f'balances/{inv}'), f"{inv} balance before")
tezos.wait(nyx.token.ci.transferFrom([{
    'tr_from': inv,
    'tr_to': tr_to,
    'amount': 3,
}]))
LOG(nyx.token.ci.big_map_get(f'allow_transfer_from/{authority}'), "Allowances after")
LOG(nyx.token.ci.big_map_get(f'balances/{tr_to}'), f"{authority} balance after")
LOG(nyx.token.ci.big_map_get(f'balances/{inv}'), f"{inv} balance after")
```

``` example
=== Allowances before ===

{'amount': 5, 'transfer_for': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'}


=== tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv balance before ===

5


=== tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx balance before ===

5


=== Allowances after ===

{'amount': 2, 'transfer_for': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'}


=== tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv balance after ===

8


=== tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx balance after ===

2
```

As you can see, the issuer contract has kept track of the number of
investors who posses some amount of the token:

``` python
nyx.log_storage()
```

``` example
=== KYC storage ===

{'members': 257, 'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx'}


=== Issuer storage ===

{ 'accounts': 258,
  'country_counters': 259,
  'country_restrictions': 260,
  'document_hashes': 261,
  'global_invest_limit': 1000,
  'global_restriction': False,
  'investor_counter': 2,
  'kyc_registrars': {'KT1Rp22phiErygFwej2dp9WBkYnNZwvhhyAi': False},
  'name': 'IssuerContract',
  'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx',
  'security_tokens': {'KT1UMF1DJQZhkh8A3DCr9VwzHGCqzWLa3a55': False}}


=== Token storage ===

{ 'agreement_procedure': False,
  'allow_transfer_from': 262,
  'balances': 263,
  'issuer': 'KT1KDDfmoBLJx3KE9sU9ciKeS49Weue65tZg',
  'name': 'Token',
  'owner': 'tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx',
  'symbol': 'NYX',
  'tokens': 990,
  'total_supply': 10}


<pynyx.environments.TokenEnv object at 0x7fae63d20d00>
```
