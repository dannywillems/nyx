# KYC

| Interface Name     | Signature                                      | Permissions |
| ------------------ | ---------------------------------------------- | ----------- |
| `addMembers`       | `investor * investor_info`                     | `O`         |
| `removeMembers`    | `investor list`                                | `O`         |
| `getMember`        | `investor * (investor_info -> operation list)` | `P`         |
| `checkMember`      | `investor list * country_restrictions`         | `I`         |
| `setMemberCountry` | `investor * country_code`                      | `O`         |

Where:

  - `investor` is a tezos address,
  - `country_code` is an hexadecimal code associated with a country, as
    defined in `pynyx/country-and-region-codes.csv`,
  - `investor_info` matches `{country: country_code ; region:
    region_code ; rating: nat ; expires: timestamp ; restricted: bool}`,
  - `country_restrictions` matches `{country_invest_limit: nat ;
    min_rating: nat ; rating_restrictions: rating_restrictions_t}`,
      - `rating_restrictions_t` is a map from ratings (`nat`) to limits
        (`nat`),
  - `O` is the contract owner,
  - `I` is a deployed issuing entity contract,
  - `T` is an unrestricted token associated with the issuer,
  - `K` is an unrestricted KYC registrar associated with the issuer.

# Issuing Entity

| Interface Name              | Signature                                                                                | Permissions |
| --------------------------- | ---------------------------------------------------------------------------------------- | ----------- |
| `updateGlobalLimit`         | `limit`                                                                                  | `O`         |
| `updateCountryRestrictions` | `(country_code * country_restrictions) list`                                             | `O`         |
| `addToken`                  | `token`                                                                                  | `O`         |
| `setRegistrar`              | `registrar * is_restricted`                                                              | `O`         |
| `setAccount`                | `account * is_restricted`                                                                | `O`         |
| `setToken`                  | `token * is_restricted`                                                                  | `O`         |
| `checkTransfer`             | `investor * investor`                                                                    | `T`         |
| `transferTokens`            | `{ from: investor ; to: investor ; zero_balances: (is_from_now_zero , is_to_now_zero) }` | `T`         |
| `modifyTokenTotalSupply`    | `{ was_balance_zero: bool , is_balance_zero: bool ; investor: investor }`                | `T`         |
| `removeCountryRestriction`  | `country_code`                                                                           | `O`         |
| `setGlobalRestriction`      | `is_restricted`                                                                          | `O`         |
| `updateCountryCounter`      | `{ country : country_code ; decrement : should_decrement }`                              | `K`         |

Where:

  - `limit` is a `nat`
  - `country_code` is an hexadecimal code associated with a country, as
    defined in `pynyx/country-and-region-codes.csv`,
  - `country_restrictions` matches `{country_invest_limit: nat ;
    min_rating: nat ; rating_restrictions: rating_restrictions_t}`,
      - `rating_restrictions_t` is a map from ratings (`nat`) to limits
        (`nat`),
  - `token` is the address of a deployed Nyx security token,
  - `registrar` is the address of a deployed KYC registrar token,
  - `is_restricted` is a boolean,
  - `is_from_now_zero` is a boolean,
  - `is_to_now_zero` is a boolean,
  - `should_decrement` is a boolean,
  - `O` is the contract owner,
  - `T` is an unrestricted token associated with the issuer,
  - `K` is an unrestricted KYC registrar associated with the issuer.

# Security Token

| Interface Name         | Signature                                                              | Permissions |
| ---------------------- | ---------------------------------------------------------------------- | ----------- |
| `burn`                 | `{tr_to: address ; amount: nat}`                                       | `O`, `I`    |
| `mint`                 | `{tr_to: address ; amount: nat}`                                       | `O`, `I`    |
| `setAllowTransferFrom` | `{tr_from: address ; tr_to: address ; amount: nat ; prev_amuont: nat}` | `i`         |
| `transfer`             | `[{tr_to: address ; amount: nat}]`                                     | `i`         |
| `transferFrom`         | `[{tr_from: address ; tr_to: address ; amount: nat}]`                  | `*`         |

Where:

  - `O` is the contract owner,
  - `i` is an investor with an account,
  - `I` is a deployed issuing entity contract associated with the token,
  - `*` is anyone,

Note:

  - `setAllowTransferFrom` uses the `prev_amount` key of its argument to
    work around the following [security
    breach](https://docs.google.com/document/d/1YLPtQxZu1UAvO9cZ1O2RPXBbT0mooh4DYKjA_jp-RLM/edit#).

# Mini Nyx

| Interface Name         | Signature                                                              | Permissions |
| ---------------------- | ---------------------------------------------------------------------- | ----------- |
| `burn`                 | `{tr_to: address ; amount: nat}`                                       | `O`         |
| `mint`                 | `{tr_to: address ; amount: nat}`                                       | `O`         |
| `setAllowTransferFrom` | `{tr_from: address ; tr_to: address ; amount: nat ; prev_amuont: nat}` | `i`         |
| `transfer`             | `[{tr_to: address ; amount: nat}]`                                     | `i`         |
| `transferFrom`         | `[{tr_from: address ; tr_to: address ; amount: nat}]`                  | `P`         |

Where:

  - `O` is the contract owner,
  - `i` is an investor with an account,
  - `P` is anyone.

Note:

  - `setAllowTransferFrom` uses the `prev_amount` key of its argument to
    work around the following [security
    breach](https://docs.google.com/document/d/1YLPtQxZu1UAvO9cZ1O2RPXBbT0mooh4DYKjA_jp-RLM/edit#).

# Crowdsale

| Interface Name   | Signature                                | Permissions |
| ---------------- | ---------------------------------------- | ----------- |
| `isOpen`         | `unit` (fails if not opened)             | `P`         |
| `canParticipate` | `address`                                | `P`         |
| `payable`        | `token * token * nat`                    | `P`         |
| `set_currency`   | `currency * fiat_rate * amount * amount` | `O`         |

Where:

  - `currency` is a Nyx security token or Mini Nyx token address,
  - `fiat~rate` is a `nat`,
  - `amount` is a `nat`,
  - `O` is the contract owner,
  - `P` is anyone.

# DvP

| Interface Name | Signature                            | Permissions |
| -------------- | ------------------------------------ | ----------- |
| `makeOrder`    | `order_info`                         | `P`         |
| `takenOrder`   | `{take_for: address; order_id: nat}` | `P`         |

Where:

  - `order_info` is of the form `{sender: address ; token_from : token ;
    amount_from : nat ; token_to : token ; amount_to : nat ; deadline:
    timestamp}` where:
      - `token` is a Nyx security token or Mini Nyx token address,
  - `order_id` is represented by a `nat` corresponding to the key of an
    order in the contract's storage's order map.
  - `take_for` is the address of the investor taking the order. This
    allows for the DvP contract owner to be able to take orders on
    behalf on investors.
  - `P` is anyone.

# Event Sink

| Interface Name  | Signature               | Permissions |
| --------------- | ----------------------- | ----------- |
| `setPermission` | `contract * permission` | `O`         |
| `postEvent`     | `string`                | `A`         |
| `FlushMessages` | `unit`                  | `O`         |

Where:

  - `contract` is the address of a contract to allow posting events on
    this sink,
  - `permission` is a boolean,
  - `O` is the contract owner,
  - `A` is any permitted contract (using `setPermission`).
